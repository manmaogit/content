package mq;


import mq.save.ResultTxtEventFactory;
import mq.save.SaveUserGeneHandler;

import org.springframework.beans.BeanUtils;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;


public class TestMQ{

    private static Disruptor<StringEvent> disruptor=null;
    
    private static DisruptorFactory<StringEvent> disruptorFactory=new DisruptorFactory<StringEvent>();


    public  void sendToGeneCountMQ(StringEvent resultTxt,boolean skip) {
        if(disruptor == null){
            initDisruptor();
        }
        RingBuffer<StringEvent> ringBuffer = disruptor.getRingBuffer();
         long sequence = ringBuffer.next(); 
        try{
            StringEvent resultTxtTarget = ringBuffer.get(sequence); // 返回可用位置的元素
            BeanUtils.copyProperties(resultTxt, resultTxtTarget); 
        }catch(Exception e){
        	e.printStackTrace();
        }finally{
        	  ringBuffer.publish(sequence); //发布
        }
    }
    
    
    public  TestMQ() {
    	 initDisruptor();
	}
  
    @SuppressWarnings("unchecked")
    private void initDisruptor(){   //初始化Disruptor队列
        disruptor=disruptorFactory.newInstance(new ResultTxtEventFactory());
        disruptor.handleEventsWith(new SaveUserGeneHandler());
        disruptor.start();//启动disruptor的线程
    }
    
    public static void main(String args[]) throws InterruptedException{
    	TestMQ sr=new TestMQ();
    	
    	for(int i=1;i<=6;i++){
    		new Thread(new Worker3(sr,i)).start();
    		
    	}
    	while(true);
    }



  
}
