package mq;

public class StringEvent {

	private String str;
	
	
	public StringEvent(){}
	
	public StringEvent(String str){
		this.str=str;
	}
	
	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}
}
