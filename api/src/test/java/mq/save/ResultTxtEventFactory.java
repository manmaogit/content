package mq.save;


import mq.StringEvent;

import com.lmax.disruptor.EventFactory;

public class ResultTxtEventFactory implements EventFactory<StringEvent>{

    @Override
    public StringEvent newInstance() {
        return new StringEvent();
    }
}
