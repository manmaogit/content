package mq.save;


import mq.StringEvent;

import org.springframework.stereotype.Component;

import com.lmax.disruptor.EventHandler;

/**
 * 保存用户基因结果，disruptor
 * @author 满茂
 *
 */
@Component
public class SaveUserGeneHandler implements EventHandler<StringEvent>{

    @Override
    public void onEvent(StringEvent str, long sequence, boolean endOfBatch)
        throws Exception {
    	System.out.println("consumer disruptor-->"+sequence);
      // System.out.println(str.getStr());
       Thread.sleep(2000);
    }
    
}
