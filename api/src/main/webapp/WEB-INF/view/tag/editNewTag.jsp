<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add New Tag</title>

<link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-theme.min.css">

<script src="../assets/jquery/jquery.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="../assets/plugins/jquery-bootpag/jquery.bootpag.js"></script>

</head>
<body>
	<div class="container">
	
		<div class="row">
			<form id="addNewTagForm">
				<table class="table" width="500" height="100" border="1px"
					bordercolor="#FFFFFF">
					<tr>
						<td style="text-align: center; font-size: 18px;">tagId</td>
						<td style="text-align: center; font-size: 18px;">
						<input
							class="form-control" type="text" value="${tag.tagId}" id="tagId" name="tagId" readonly="readonly" /></td>
					</tr>
					
					<tr>
						<td style="text-align: center; font-size: 18px;">tagName</td>
						<td style="text-align: center; font-size: 18px;">
						<input
							class="form-control" type="text" value="${tag.tagName}" id="tagName" name="tagName" /></td>
					</tr>
					
					<tr>
						<td style="text-align: center; font-size: 18px;">Category</td>
						<td style="text-align: center; font-size: 18px;">
						<select
							class="form-control" name="categoryCode">
								<option value="0"></option>
								<c:forEach var="cate" items="${categoryList}">
									<c:choose>
										<c:when test="${cate.categoryCode == tag.categoryCode}">
											<option value="${cate.categoryCode}" selected>${cate.categoryName}</option>
    									</c:when>
										<c:otherwise>
										   <option value="${cate.categoryCode}">${cate.categoryName}</option>
    									</c:otherwise>
									</c:choose>
								</c:forEach>
						</select>
						</td>
					</tr>
					
					<tr>
						<td style="text-align: center; font-size: 18px;">documentFrequency</td>
						<td style="text-align: center; font-size: 18px;"><input
							class="form-control" type="text" value="${tag.documentFrequency}"
							name="documentFrequency" /></td>
					</tr>
					
					<tr>
						<td style="text-align: center; font-size: 18px;">priority</td>
						<td style="text-align: center; font-size: 18px;">
							<input class="form-control" name="priority" value="${tag.priority}" />
						</td>
					</tr>
				</table>
			</form>
		</div>
		
		<div class="row">
			<div class='col-md-6 col-md-offset-6' style='margin-left:50%'>
				<input type="button" class="btn btn-primary" onclick="addNewTag()" value=" Modify " />
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
	
		function addNewTag() {
			var formData = $('#addNewTagForm').serialize();
			$.ajax({
				url : 'saveOrUpdate?',// 跳转到 action  
				data : formData,
				type : 'post',
				cache : false,
				dataType : 'json',
				success : function(data) {
					alert("code:\n\t\t" + data.code + "\nmessage:\n\t\t"
							+ data.msg + "\n");
				},
				error : function(e) {
					alert("连接异常！" + e);
				}
			});
			return false;
		}
	</script>
	
</body>
</html>