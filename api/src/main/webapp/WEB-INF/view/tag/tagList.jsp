<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tag</title>

<link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css">

<script src="../assets/jquery/jquery.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="../assets/plugins/jquery-bootpag/jquery.bootpag.js"></script>
</head>

<body>
	<div class="container">
	
		<form id="queryForm" method="post" action="list" class="form-inline" style="margin: 20px 0;">
			<label>TagName&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<input type="text" name="tagName"
				class="form-control"  value="${tagName}" />
			<label style="margin-left: 20px">Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<select style="margin-left: 20px" class="form-control" name="categoryCode">
				<option value="">All</option>
				<c:forEach var="cate" items="${categoryList}">
					<c:choose>
						<c:when test="${cate.categoryCode == categoryCode}">
							<option value="${cate.categoryCode}" selected>${cate.categoryName}</option>
						</c:when>
						<c:otherwise>
							<option value="${cate.categoryCode}">${cate.categoryName}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
			
			<input id="pageNo" name='pageNo' class="hidden" value="${pageNo}" />
			
			<input type="button" class="btn btn-primary" onclick="search()" style="margin-left: 20px" onclick="" value="查询">
				
			<input type="button" class="btn btn-primary"
				style="float:right" onclick="addNewTagWindow()" value="添加">
		</form>
		
		<div>
			<table  class="table table-striped table-bordered table-hover" style="border: 1px solid grey">
				<thead>
					<tr>
						<th>TagId</th>
						<th>TagName</th>
						<th>Category</th>
						<th>DocumentFrequency</th>
						<th>Priority</th>
						<th>Operation</th>
					</tr>
				 </thead>
				 
				<tbody id="tbody">
					<c:forEach var="tag" items="${tagList}">
					  <tr>
   						<td style="width: 90px">${tag.tagId}</td>
   						<td style="max-width: 400px">${tag.tagName}</td>
   						<td>${tag.categoryName}</td>
   						<td>${tag.documentFrequency}</td>
   						<td>${tag.priority}</td>
   						<td>
   							<a href="#" onclick="editTagWindow('${tag.tagId}','${tag.categoryCode}')">Edit</a>
   							<br>
   							<a href="#" onclick="deleteTag('${tag.tagId}','${tag.tagName}','${tag.categoryCode}')">Delete</a>
   						</td>
   					  </tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		
		<div class="table_paginate">
			共 <span id="totalPage">${totalPage}</span> 页 [<span id="totalCount">${totalCount}</span> 条]<br>
		</div>
		
		<!-- 弹窗展示 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel"></h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
		
		<!-- 总共多少条 -->
		<span id="totalCount" class="hidden">${totalCount}</span>
		<!-- 总共多少页 -->
		<span id="totalPage" class="hidden">${totalPage}</span>
		<!-- 当前页面页号 -->
		<span id="pageNoVal" class="hidden">${pageNo}</span>
		
		<script type="text/javascript">
		
			function showModal(title, content, isHtml) {
				$('#myModal .modal-title').text(title);
				if (isHtml) {
					$('#myModal .modal-body').html(content);
				} else {
					$('#myModal .modal-body').text(content);
				}
				$('#myModal').modal();
			}
			
			function addNewTagWindow() {
				var html = "<iframe src='/cms/tag/addTagPage' width='800' height='400'  frameborder='no' ></iframe>";
				showModal('添加标签', html, true);
			}

			function editTagWindow(tagId, categoryCode) {
				var html = "<iframe src='/cms/tag/editTagPage?tagId="
						+ tagId
						+ "&categoryCode="
						+ categoryCode
						+ "'  width='800' height='400'  frameborder='no' ></iframe>";
				showModal('编辑标签', html, true);
			}
			
			/*查询*/
			function search(){
				$('#pageNo').val(1);
				$('#queryForm').submit();	
			}
			
			/*删除*/
			function deleteTag(tagId, tagName, categoryCode) {
				if (window.confirm('你确定要删除tag: ' + tagName)) {
					$.ajax({
						url : 'deleteTag',
						data : {
							tagId : tagId,
							categoryCode : categoryCode
						},
						type : 'GET',
						cache : false,
						dataType : 'json',
						success : function(data) {
							window.location.reload(true);
						},
						error : function(e) {
							alert("连接异常！");
						}
					});
				} else {
				}
			}
			
			/*分页*/
			$(function(){
				$('.table_paginate').bootpag().on('page', function(event, num) {
					toPage(num);
				});
				
				$('.table_paginate').bootpag({
					total : $('#totalPage').text(),
					page : $('#pageNoVal').text(),
					maxVisible : 10,
					prev : '上一页',
					next : '下一页'
				});
			});
			
			function toPage(num) {
				$('#pageNo').val(num);
				$('#queryForm').submit();
				//可以在这里用ajax请求json然后来设置表格值
			}
			
			
		</script>

	</div>
</body>

</html>