package com.content.help.controller;

import com.common.vo.response.GenericResponse;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by maoman on 16-9-22.
 */
@Controller
public class AjaxController {
    @RequestMapping(value = "/ajax",method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse ajax(HttpServletRequest request,@RequestBody @Validated User user,BindingResult result){
        if(result.hasErrors()){
            System.out.println("error");
        }
        System.out.println(user.getB());
        return new GenericResponse();
    }


    @RequestMapping(value = "/ajax_one",method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse ajax(HttpServletRequest request,int a,String b){
        System.out.println(a);
        System.out.println(b);
        return new GenericResponse();
    }
}
