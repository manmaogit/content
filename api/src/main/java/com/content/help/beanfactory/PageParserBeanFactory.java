package com.content.help.beanfactory;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by maoman on 16-9-21.
 */
@Component
public class PageParserBeanFactory implements ApplicationContextAware{

    private static Map<String, PageParser> listParserMap = new HashMap<String, PageParser>();

    public  static PageParser getParser(String name){
        return  listParserMap.get(name);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        initParserMap(applicationContext);
    }

    private void initParserMap(ApplicationContext applicationContext) {
    	
        //获取所有实现了PageParser接口类的实例对象
        Map<String,PageParser> map = applicationContext.getBeansOfType(PageParser.class);

        for (PageParser parser : map.values()) {
            String name = parser.getParserName();
            listParserMap.put(name, parser);
        }
    }
}
