package com.content.help.beanfactory;

import com.content.help.beanfactory.PageParser;
import org.springframework.stereotype.Component;

/**
 * Created by maoman on 16-9-21.
 */
@Component
public class BaiduPageParser implements PageParser {
    @Override
    public String getParserName() {
        return "BaiduPageParser";
    }
}
