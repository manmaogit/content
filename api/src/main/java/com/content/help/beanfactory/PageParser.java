package com.content.help.beanfactory;

/**
 * Created by maoman on 16-9-21.
 */
public interface PageParser {
    public String getParserName();
}
