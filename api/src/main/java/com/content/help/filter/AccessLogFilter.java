package com.content.help.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AccessLogFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(AccessLogFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String reqUri = req.getRequestURI();

        String qs = req.getQueryString() == null ? "" : req.getQueryString();
        String ct = req.getContentType() == null ? "" : req.getContentType();


        LOG.info("{} {} {} {} {}", req.getRemoteAddr(), req.getMethod(), reqUri, qs, ct);

    /*    // 当请求地址不是请求登录URL时
        if (reqUri.startsWith("/web/") && !reqUri.equals("/web/login")) {

            Integer operatorId = getOpIdFromCookie(req);

            if (operatorId == null) { // 数据为空
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().print("{\"code\":401,\"msg\":\"请登录\"}");
                return;
            }

            request.setAttribute(Constants.ATTR_OPERATOR_ID, operatorId);
            updateLoginCookie(response, operatorId); // 更新operatorId

        }*/

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

}
