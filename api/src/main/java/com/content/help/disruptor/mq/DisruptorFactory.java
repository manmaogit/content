package com.content.help.disruptor.mq;


import java.util.concurrent.ThreadFactory;

import org.springframework.stereotype.Component;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

@Component
public class DisruptorFactory<T>{
    
    //指定RingBuffer的大小
    private static int bufferSize = 1024*64;
    
    //等待策略
    private static WaitStrategy defaultStrategy = new BlockingWaitStrategy();
    
    //生产者类型，多生产者模式
    private static ProducerType producerType=ProducerType.MULTI;
    
    
    public  Disruptor<T> newInstance(EventFactory<T> factory){
        return new Disruptor<T>(factory,bufferSize, getDefaultThreadFactory(), producerType, defaultStrategy);
    }
    
    
    
    private ThreadFactory getDefaultThreadFactory(){
        // 生产者的线程工厂
        ThreadFactory threadFactory = new ThreadFactory(){
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "simpleThread");
            }
        };
        return threadFactory;
    }
}
