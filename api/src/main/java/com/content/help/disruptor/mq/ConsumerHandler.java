package com.content.help.disruptor.mq;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.WorkHandler;

/**
 * 保存用户基因结果，disruptor
 * @author 
 *
 */
public class ConsumerHandler implements EventHandler<Message>,WorkHandler<Message>{
    
    private int no;

    
    public ConsumerHandler() {
       
    }
    
    public ConsumerHandler(int no) {
        this.no=no;
    }
    
    @Override
    public void onEvent(Message msg, long sequence, boolean endOfBatch)
        throws Exception {
        System.out.println(no+"  data commming......"+msg.getId());
    }

    @Override
    public void onEvent(Message msg)
        throws Exception {
        System.out.println(no+"  data commming......"+msg.getId());
    }
    
    
}
