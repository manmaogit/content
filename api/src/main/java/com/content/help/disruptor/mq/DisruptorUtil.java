package com.content.help.disruptor.mq;

import javax.annotation.Resource;

import com.lmax.disruptor.EventTranslatorOneArg;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;

@Component
public class DisruptorUtil implements  InitializingBean,DisposableBean{

    @Resource
    private DisruptorFactory<Message> disruptorFactory;

    
    private static Disruptor<Message> disruptor=null;

    private  Translator translator=new Translator();

    /**
     * 发布消息到消息队列 方法1
     * @param msg
     */
    public void sendMessageToMQ(Message msg) {
        if(disruptor == null) {
            initDisruptor();
        }
        RingBuffer<Message> ringBuffer = disruptor.getRingBuffer();
        long sequence = ringBuffer.next(); 
        try{
            Message msgTarget = ringBuffer.get(sequence); // 返回可用位置的元素
            BeanUtils.copyProperties(msg, msgTarget);
        }finally{
            ringBuffer.publish(sequence); //发布
        }


    }


    /**
     * 发布消息到消息队列 方法2
     * @param msg
     */
    public void sendMessageToMQ2(Message msg){
        if(disruptor == null) {
            initDisruptor();
        }
        RingBuffer<Message> ringBuffer = disruptor.getRingBuffer();
        ringBuffer.publishEvent(translator,msg);
    }


    /**
     * 发布消息到消息队列 公共方法
     * @param msg
     */
    public <T> void sendMessageToMQCommon(Disruptor<T> disruptor,T msg){
        if(disruptor == null) {
            return;
        }
        RingBuffer<T> ringBuffer = disruptor.getRingBuffer();
        long sequence = ringBuffer.next();
        try{
            T msgTarget = ringBuffer.get(sequence); // 返回可用位置的元素
            BeanUtils.copyProperties(msg, msgTarget);
        }finally{
            ringBuffer.publish(sequence); //发布
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initDisruptor();
    }

    @SuppressWarnings("unchecked")
    private void initDisruptor(){   //初始化Disruptor队列
        disruptor=disruptorFactory.newInstance(new MessageEventFactory());
        disruptor.handleEventsWith(new ConsumerHandler(1),new ConsumerHandler(2));
        //disruptor.handleEventsWithWorkerPool(new ConsumerHandler(1),new ConsumerHandler(2));
        disruptor.start();//启动disruptor的线程
    }

    @Override
    public void destroy() throws Exception {
        disruptor.shutdown();
    }

    static class Translator implements EventTranslatorOneArg<Message, Message> {
        @Override
        public void translateTo(Message event, long sequence, Message data) {
            event.setId(data.getId());
        }
    }
    
}
