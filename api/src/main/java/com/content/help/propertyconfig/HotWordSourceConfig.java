package com.content.help.propertyconfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;


@Component
public class HotWordSourceConfig implements InitializingBean {

    private Logger LOG = LoggerFactory.getLogger(HotWordSourceConfig.class);

    private static final String urlPrefix =
            "https://www.google.ru/trends/api/stories/latest?tz=-480&fi=15&fs=15&ri=300&rs=15&sort=0";
    
    private static List<HotWordSource> hotWordSourceList = new ArrayList<HotWordSource>();
    
    
    @Override
    public void afterPropertiesSet() throws Exception {
        try {
            //读取配置文件，并且解析放置到对象中
            Properties prop = new Properties();
            prop.load(this.getClass().getResourceAsStream("/hotWordSource.properties"));

            for (Entry<Object, Object> entry : prop.entrySet()) {
                String key = (String) entry.getKey();
                String value = (String) entry.getValue();
                
                HotWordSource hotWordSource=new HotWordSource();
                String sourceUrl=key.replace("_", ":");
                hotWordSource.setSourceUrl(sourceUrl);

                String str[]=value.split(";");
                
                if(str.length==3){
                    
                    String country=str[0];
                    String language=str[1];
                    String category=str[2];
                    
                    hotWordSource.setCategory(category);
                    hotWordSource.setLanguage(language);
                    hotWordSource.setCountry(country);
                    
                    String url=urlPrefix+"&hl="+language+"-"+country+"&cat="+category+"&geo="+country;
                    
                    hotWordSource.setJsonUrl(url);
                    
                    hotWordSourceList.add(hotWordSource);
                    
                }else {
                    continue;
                }
                
                        
            }

        } catch (IOException e) {
            LOG.error("read and parse spider-config.properties failed.", e);
            return;
        }

    }

    public List<HotWordSource> getHotWordList() {
        return hotWordSourceList;
    }

}
