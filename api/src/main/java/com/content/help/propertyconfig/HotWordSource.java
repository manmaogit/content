package com.content.help.propertyconfig;

import java.io.Serializable;

public class HotWordSource implements Serializable{
    
    private static final long serialVersionUID = -1096997886743491852L;
    
    private String sourceUrl; //热搜词源url
    
    private String country;   //RU,IN,US ,国家两位大写字母简写
    
    private String language;  //en,ru，语言两位字母小写
    
    private String category;  //分类，s(Sport) e(Stars) b(Economics)
    
    private String  jsonUrl;  //请求json的url
    
    
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getJsonUrl() {
        return jsonUrl;
    }

    public void setJsonUrl(String jsonUrl) {
        this.jsonUrl = jsonUrl;
    }
    
}
