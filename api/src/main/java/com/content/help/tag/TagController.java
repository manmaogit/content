/*
package com.content.help.tag;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.common.vo.page.Pagination;
import com.content.help.tag.vo.TagVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;

*/
/*
 *　管理接口类
 * @author maoman
 *
 *//*

@Controller
public class TagController {
    
    @Resource
    private CategoryService categoryService;
    
    @Resource
    private CategoryTagStatService categoryTagStatService;
    
    private static Logger logger = LoggerFactory.getLogger(TagController.class);
    
    @RequestMapping(value = "/tag/list")
    public ModelAndView tagList(HttpServletRequest request,String tagName,String categoryCode,Integer pageNo) {
        
        if(pageNo==null||pageNo<1)
            pageNo=1;
        Pagination pagination=new Pagination(pageNo,20);
        
        */
/*列表*//*

        List<CategoryTagStatInfo> infos=categoryTagStatService.findCateTagStatList(tagName, categoryCode,pagination);
        List<TagVo>tagList=convertList(infos);
        System.out.println(JSON.toJSON(tagList));
        */
/*分类列表*//*

        List<CategoryItem> categoryList=categoryService.getCategoryList();
        
        */
/*查询文档总数*//*

        Long totalCount=categoryTagStatService.count(tagName, categoryCode);
        Long totalPage=totalCount/20;
        if(totalCount%20!=0){
            totalPage +=1;
        }
        
        */
/*返回view,带参数*//*

        ModelAndView modelAndView=new ModelAndView("tag/tagList");
        modelAndView.addObject("categoryList",categoryList);
        modelAndView.addObject("tagList",tagList);
        
        modelAndView.addObject("tagName", tagName);
        modelAndView.addObject("categoryCode", categoryCode);
        modelAndView.addObject("pageNo", pageNo);
        modelAndView.addObject("totalCount",totalCount);
        modelAndView.addObject("totalPage",totalPage);
        
        return modelAndView;
    }
    
    @RequestMapping(value = "/tag/addTagPage", method = RequestMethod.GET)
    public ModelAndView addTagPage(HttpServletRequest request){
        
        List<CategoryItem> categoryList=categoryService.getCategoryList();
        ModelAndView modelAndView=new ModelAndView("tag/addNewTag");
        modelAndView.addObject("categoryList",categoryList);
        
        return modelAndView;
    }
    
    
    @RequestMapping(value = "/tag/editTagPage", method = RequestMethod.GET)
    public ModelAndView editTagPage(HttpServletRequest request,String tagId,String categoryCode){
        
        CategoryTagStatInfo categoryTagStatInfo=categoryTagStatService.findOneCateTagStat(tagId, categoryCode);
        logger.info(JSON.toJSONString(categoryTagStatInfo));
        
        TagVo tagVo=convert(categoryTagStatInfo);
        
        List<CategoryItem> categoryList=categoryService.getCategoryList();
        ModelAndView modelAndView=new ModelAndView("tag/editNewTag");
        modelAndView.addObject("categoryList",categoryList);
        modelAndView.addObject("tag",tagVo);
        return modelAndView;
    }
    
    @RequestMapping(value = "/tag/saveOrUpdate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseBase saveOrUpdate(HttpServletRequest request,CategoryTagStatInfo info){  
        //来自爬虫的tag
        categoryTagStatService.saveCategoryTagStatInfo(info,1);
        return ResponseBuilder.baseResponse(200, "ok");
    }
    
    @RequestMapping(value = "/tag/deleteTag", method = RequestMethod.GET)
    @ResponseBody
    public ResponseBase deleteTag(HttpServletRequest request,String tagId,String categoryCode){
        categoryTagStatService.deleteCategoryTagStat(tagId, categoryCode);
        return ResponseBuilder.baseResponse(200, "ok");
    }
    
    
    private  List<TagVo> convertList(List<CategoryTagStatInfo> infos){
        List<TagVo>tagList=new ArrayList<TagVo>();
        for(CategoryTagStatInfo info:infos){
            tagList.add(convert(info));
        }
        return tagList;
    }
    
   
    private TagVo convert(CategoryTagStatInfo info){
        TagVo tagVo=new TagVo();
        tagVo.setCategoryCode(info.getCategoryCode());
        tagVo.setDocumentFrequency(info.getDocumentFrequency());
        tagVo.setPriority(info.getPriority());
        tagVo.setTagId(info.getTagId());
        tagVo.setTagName(info.getTagName());
        Category category=categoryService.findByCategoryCode(info.getCategoryCode());
        if(category!=null)
            tagVo.setCategoryName(category.getName());
        return tagVo;
    }
    
    
    
     
}
*/
