package com.content.help.tag.vo;

public class TagVo {

    
    private Long tagId;
    private String tagName;
    private String categoryCode;
    private String  categoryName;
    private Long documentFrequency;
    private Integer priority;
    
    public Long getTagId() {
        return tagId;
    }
    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }
    public String getTagName() {
        return tagName;
    }
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
    public String getCategoryCode() {
        return categoryCode;
    }
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }
    public Long getDocumentFrequency() {
        return documentFrequency;
    }
    public void setDocumentFrequency(Long documentFrequency) {
        this.documentFrequency = documentFrequency;
    }
    public Integer getPriority() {
        return priority;
    }
    public void setPriority(Integer priority) {
        this.priority = priority;
    }
    public String getCategoryName() {
        return categoryName;
    }
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    
    
    
}
