package com.content.biz.redis;

import java.util.Collection;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisManager {

	@Autowired
	@Qualifier("redisTemplate")
	private RedisTemplate<String, String> redisTemplate;

	public void set(String key, String value){
		this.redisTemplate.opsForValue().set(key, value);
	}
	
	public String get(String key){
		return this.redisTemplate.opsForValue().get(key);
	}
	
    public  String getAndSet(String key, String value) {
        if (value == null) {
            return this.get(key);
        }
        return redisTemplate.opsForValue().getAndSet(key, value);
    }

	public void remove(String key){
		this.redisTemplate.delete(key);
	}
	
	public Long incrBy(String key, Long value){
		return this.redisTemplate.opsForValue().increment(key, value);
	}
	
	public Long decrBy(String key, Long value){
		return this.redisTemplate.opsForValue().increment(key, 0-value);
	}
	
	
	
	/**
	 * 返回member的score
	 * @param key
	 * @param member
	 * @return
	 */
	public Double zscore(String key, String member){
		return this.redisTemplate.opsForZSet().score(key, member);
	}
	
	/**
	 * 往有序集合添加元素
	 * @param key
	 * @param member
	 * @param score
	 * @return
	 */
	public Boolean zset(String key, String member, Long score){
		return this.redisTemplate.opsForZSet().add(key, member, score);
	}
	
	/**
	 * 移除指定member
	 * @param key
	 * @param member
	 * @return
	 */
	public Long zremove(String key, String member){
		return this.redisTemplate.opsForZSet().remove(key, member);
	}
	
	/**
	 * 按score从小到大排序返回member的排名
	 * @param key
	 * @param member
	 * @return
	 */
	public Long zrank(String key, String member){
		return this.redisTemplate.opsForZSet().rank(key, member);
	}
	
	/**
	 * 返回排名介于start和end之间的member
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public Set<String> zrange(String key, long start, long end){
		return this.redisTemplate.opsForZSet().range(key, start, end);
	}
	
	/**
	 * 移除排名介于start和end之间的member
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public Long zremoveRange(String key, long start, long end){
		return this.redisTemplate.opsForZSet().removeRange(key, start, end);
	}
	
	public Long leftPush(String key, String value){
		return this.redisTemplate.opsForList().leftPush(key, value);
	}
	
	public Long leftPushAll(String key, Collection<String> values){
		return this.redisTemplate.opsForList().leftPushAll(key, values);
	}
	
	public String rightPop(String key){
		return this.redisTemplate.opsForList().rightPop(key);
	}
	
	
	/**
	 * hash操作
	 * @param key
	 * @param hashKey
	 */
	
	public Long hget(String key,String hashKey){
	    HashOperations<String, String, String> hashOper=this.redisTemplate.opsForHash();
	    String obj=hashOper.get(key, hashKey);
	    if(obj != null)
	        return Long.parseLong(obj);
	    else 
	        return  null;
	}
	
	public void hset(String key,String hashKey,Long value){
	    if(value != null)
	        this.redisTemplate.opsForHash().put(key, hashKey, value.toString());
	}
	
	public Long hashIncrease(String key,String hashKey,Long value){
	    return this.redisTemplate.opsForHash().increment(key, hashKey, value);
	}
	
	public Long hashDecrease(String key,String hashKey,Long value){
        return this.redisTemplate.opsForHash().increment(key, hashKey,  0-value);
    }
	
	
	
}