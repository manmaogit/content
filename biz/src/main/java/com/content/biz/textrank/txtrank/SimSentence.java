package com.content.biz.textrank.txtrank;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 计算两个语句相关性,实验代码
 * @author maoman
 *
 */
@Deprecated
public class SimSentence{
    
    /**
     * 计算两个语句的相关性
     * @param sentence1
     * @param sentence2
     * @return 
     */
    public static double getSimSorce(String sentence1,String sentence2){
        
        double wordCount=0;
        
        List<String> list1 = removeStopWord(TextUtil.spiltWord(sentence1)); //分解句子为单词
        List<String> list2 = removeStopWord(TextUtil.spiltWord(sentence2)); //分解句子为单词
        
        for(String t:list1){
            if(list2.contains(t)){
                wordCount++;
            }
        }
        
        return wordCount/(Math.log(list1.size())+Math.log(list2.size()));
    }
    
    
    private static List<String> removeStopWord(List<String> list){
        List<String> result=new ArrayList<String>();
        
        for(String t:list){
            if(TextUtil.shouldInclude(t)){
                result.add(t);
            }
        }
        return result;
    }
    
    
    public static void main(String args[]){
        System.out.println(getSimSorce("Минэкономразвития предложило приватизировать Крымскую железную дорогу",
            "На примере относительно небольшой Крымской ЖД можно будет посмотреть, эффективной окажется эта мера или нет, после чего можно будет рассматривать вопрос о целесообразности этой меры в масштабах всей страны."));
    }
    
}
