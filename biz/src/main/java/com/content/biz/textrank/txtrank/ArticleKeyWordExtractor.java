package com.content.biz.textrank.txtrank;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;


public class ArticleKeyWordExtractor {

    /**
     * 阻尼系数，推荐一般取值为0.85
     */
    private final static float d = 0.85f;

    /**
     * 最大迭代次数
     */
    private final static int max_iter = 200;

    /**
     * 词组窗口大小
     */
    private final static int WORDS_WINDOW_SIZE = 8;

    /**
     * 当前分值和上次分值之差的最小值,当迭代多次后,上次分值和下次分值相差<=0.001后,迭代停止.
     */
    private static final float min_diff = 0.001f;

    /**
     * 提取关键词,设定个数
     * 
     * @param document 文档内容
     * @param size 希望提取几个关键词
     * @return 一个列表
     */
    public static List<String> getKeywordList(String document, int size) {
        return ArticleKeyWordExtractor.getKeyword(document, size);
    }

    /**
     * 提取关键词
     * 
     * @param content
     * @return
     */
    public static List<String> getKeyword(String content, int words) {
        
        Set<Map.Entry<String, Float>> entrySet = getTermAndRank(content, words).entrySet();
        
        List<String> result = new ArrayList<String>(entrySet.size());
        for (Map.Entry<String, Float> entry : entrySet) {
            result.add(entry.getKey());
        }
        return result;
    }

    /**
     * 返回全部分词结果和对应的原始rank
     * 
     * @param content
     * @return
     */
    private static Map<String, Float> getTermAndRank(String content) {
        assert content != null;
        /* List<Term> termList = defaultSegment.seg(content); */
        List<String> termList = TextUtil.spiltWord(content); // 采用分隔符分词
        Map<String, Float> resultMap=getRank(termList);
        return resultMap;
    }

    /**
     * 返回分数最高的前size个分词结果和对应的原始rank
     * 
     * @param content
     * @param size
     * @return
     */
    private static Map<String, Float> getTermAndRank(String content, Integer size) {
        
        Map<String, Float> map = getTermAndRank(content);
        
        Map<String, Float> result = new LinkedHashMap<String, Float>();
        for (Map.Entry<String, Float> entry : new MaxHeap<Map.Entry<String, Float>>(size,
                new Comparator<Map.Entry<String, Float>>() {
                    @Override
                    public int compare(Map.Entry<String, Float> o1, Map.Entry<String, Float> o2) {
                        return o1.getValue().compareTo(o2.getValue());
                    }
                }).addAll(map.entrySet()).toList()) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    /**
     * 使用已经分好的词来计算rank分值
     * 
     * @param termList
     * @return
     */
    private static Map<String, Float> getRank(List<String> termList) {
        List<String> wordList = new ArrayList<String>(termList.size());

        /* 去掉停用词 */
        for (String t : termList) {
            if (TextUtil.shouldInclude(t)) {
                wordList.add(t);
            }
        }

        /* 按照窗口大小构建词与词的关联,建立关联矩阵 */
        Map<String, Set<String>> words = new TreeMap<String, Set<String>>();
        Queue<String> que = new LinkedList<String>();
        for (String w : wordList) {
            if (!words.containsKey(w)) {
                words.put(w, new TreeSet<String>());
            }
            que.offer(w);
            if (que.size() > WORDS_WINDOW_SIZE) {
                que.poll();
            }

            for (String w1 : que) {
                for (String w2 : que) {
                    if (w1.equals(w2)) {
                        continue;
                    }

                    words.get(w1).add(w2);
                    words.get(w2).add(w1);
                }
            }
        }

        /* rank值迭代 */
        Map<String, Float> score = new HashMap<String, Float>();
        for (int i = 0; i < max_iter; ++i) // 迭代循环
        {
            Map<String, Float> m = new HashMap<String, Float>();
            float max_diff = 0;
            for (Map.Entry<String, Set<String>> entry : words.entrySet()) // 计算每个词的rank值
            {
                String key = entry.getKey();
                Set<String> value = entry.getValue();
                m.put(key, 1 - d);
                for (String element : value) {
                    int size = words.get(element).size();
                    if (key.equals(element) || size == 0) continue;
                    // 累加
                    m.put(key,
                            m.get(key) + d / size
                                    * (score.get(element) == null ? 0 : score.get(element)));
                }
                // 计算上一次的rank值和这次计算出来的rank值差值,用于控制迭代循环退出
                max_diff =
                        Math.max(max_diff, Math.abs(m.get(key)
                                - (score.get(key) == null ? 0 : score.get(key))));

            }

            score = m;
            // 如果前后计算的rank值差值小于 0.001f;则停止迭代
            if (max_diff <= min_diff) break;
        }

        return score;
    }

    public static void main(String[] args) {
        String content =
                "«Крымская железная дорога» основана после присоединения Крыма к России в 2014 году на базе подразделений Крымской дирекции Приднепровской железной дороги «Укрзализныци», расположенных на полуострове. В декабре 2015 года предприятие было передано в собственность РФ, в ведение Федерального агентства железнодорожного транспорта (Росжелдор). В январе 2016 года преобразовано во ФГУП «Крымская железная дорога»."
                        + "Железные дороги Крыма — рельсовые пути сообщения, связывающие Севастополь, Симферополь, Евпаторию, Саки, Феодосию, Керчь, Джанкой, Красноперекопск и Армянск."
                        + "В составе дороги находятся три локомотивных депо (Симферополь, Джанкой, Керчь), вагонное депо в Джанкое, два пассажирских депо с ремонтной базой, а также моторвагонное депо в Симферополе и другие объекты."
                        + "Премьер-министр РФ Дмитрий Медведев в октябре этого года поручил Минтрансу, Минпромторгу, Минэкономразвития, Минфину, главе Крыма Сергею Аксенову, врио губернатора Севастополя Дмитрию Овсянникову и президенту ОАО «Российские железные дороги» Олегу Белозерову до 1 февраля 2017 года рассмотреть вопрос об обновлении к 2020 года пассажирского и тягового подвижного состава КЖД для перевозок в пригородном сообщении, а также в поездах дальнего следования летом.";
        System.out.println(ArticleKeyWordExtractor.getKeywordList(content, 3));
    }
}
