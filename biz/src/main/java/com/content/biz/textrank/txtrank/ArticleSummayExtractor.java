/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2014/8/22 15:58</create-date>
 */
package com.content.biz.textrank.txtrank;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import com.hankcs.hanlp.utility.TextUtility;

/**
 * TextRank 自动摘要
 *
 */
public class ArticleSummayExtractor
{
    /**
     * 阻尼系数（ＤａｍｐｉｎｇＦａｃｔｏｒ），一般取值为0.85
     */
    private final static double d = 0.85;
    
    /**
     * 最大迭代次数
     */
    private final static int max_iter = 200;
    
    private final static double min_diff = 0.001;
    
    /**
     * 文档句子的个数
     */
    private int D;
    
    /**
     * 拆分为[句子[单词]]形式的文档
     */
    private List<List<String>> docs;
    
    /**
     * 排序后的最终结果 score <-> index
     */
    private TreeMap<Double, Integer> top;

    /**
     * 句子和其他句子的相关程度
     */
    private double[][] weight;
    
    /**
     * 该句子和其他句子相关程度之和
     */
    private double[] weight_sum;
    
    /**
     * 迭代之后收敛的权重
     */
    private double[] vertex;

    /**
     * BM25相似度
     */
    private BM25 bm25;
    

    public ArticleSummayExtractor(List<List<String>> docs)
    {
        this.docs = docs;
        bm25 = new BM25(docs);
        D = docs.size();
        weight = new double[D][D];
        weight_sum = new double[D];
        vertex = new double[D];
        top = new TreeMap<Double, Integer>(Collections.reverseOrder());
        solve();
    }
    
    private void solve()
    {
        int cnt = 0;
        for (List<String> sentence : docs)
        {
            double[] scores = bm25.simAll(sentence);
            weight[cnt] = scores;
            weight_sum[cnt] = sum(scores) - scores[cnt]; // 减掉自己，自己跟自己肯定最相似
            vertex[cnt] = 1.0;
            ++cnt;
        }
        for (int _ = 0; _ < max_iter; ++_)
        {
            double[] m = new double[D];
            double max_diff = 0;
            for (int i = 0; i < D; ++i)
            {
                m[i] = 1 - d;
                for (int j = 0; j < D; ++j)
                {
                    if (j == i || weight_sum[j] == 0) continue;
                    m[i] += (d * weight[j][i] / weight_sum[j] * vertex[j]);
                }
                double diff = Math.abs(m[i] - vertex[i]);
                if (diff > max_diff)
                {
                    max_diff = diff;
                }
            }
            vertex = m;
            if (max_diff <= min_diff) break;
        }
        //排序
        for (int i = 0; i < D; ++i)
        {
            top.put(vertex[i], i);
        }
    }
    
    /**
     * 获取前几个关键句子
     *
     * @param size 要几个
     * @return 关键句子的下标
     */
    public int[] getTopSentence(int size)
    {
        Collection<Integer> values = top.values();
        size = Math.min(size, values.size());
        int[] indexArray = new int[size];
        Iterator<Integer> it = values.iterator();
        for (int i = 0; i < size; ++i)
        {
            indexArray[i] = it.next();
        }
        return indexArray;
    }
    
    /**
     * 简单的求和
     *
     * @param array
     * @return
     */
    private static double sum(double[] array)
    {
        double total = 0;
        for (double v : array)
        {
            total += v;
        }
        return total;
    }
    
    
    /**
     * 将句子列表转化为文档
     *
     * @param sentenceList
     * @return
     */
    private static List<List<String>> convertSentenceListToDocument(List<String> sentenceList)
    {
        List<List<String>> docs = new ArrayList<List<String>>(sentenceList.size());
        for (String sentence : sentenceList)
        {
            List<String> termList =TextUtil.spiltWord(sentence);
            
            List<String> wordList = new LinkedList<String>();
            for (String term : termList)
            {
                if (TextUtil.shouldInclude(term))
                {
                    wordList.add(term);
                }
            }
            docs.add(wordList);
        }
        return docs;
    }
    
    
    /**
     * 
     *
     * @param document 目标文档
     * @param size     需要的关键句的个数
     * @return 关键句列表
     */
    public static List<String> getTopSentenceList(String document, int size)
    {
        List<String> sentenceList = TextUtil.spiltSentence(document);
        List<List<String>> docs = convertSentenceListToDocument(sentenceList);
        ArticleSummayExtractor textRank = new ArticleSummayExtractor(docs);
        int[] topSentence = textRank.getTopSentence(size);
        List<String> resultList = new LinkedList<String>();
        for (int i : topSentence)
        {
            resultList.add(sentenceList.get(i));
        }
        return resultList;
    }

    /**
     * 
     *
     * @param document   目标文档
     * @param max_length 需要摘要的长度
     * @return 摘要文本
     */
    public static String getSummary(String document, int max_length)
    {
        List<String> sentenceList = TextUtil.spiltSentence(document);

        int sentence_count = sentenceList.size();
        int document_length = document.length();
        int sentence_length_avg = document_length / sentence_count;
        int size = max_length / sentence_length_avg + 1;
        List<List<String>> docs = convertSentenceListToDocument(sentenceList);
        ArticleSummayExtractor textRank = new ArticleSummayExtractor(docs);
        int[] topSentence = textRank.getTopSentence(size);
        List<String> resultList = new LinkedList<String>();
        for (int i : topSentence)
        {
            resultList.add(sentenceList.get(i));
        }

        resultList = permutation(resultList, sentenceList);
        resultList = pick_sentences(resultList, max_length);
        return TextUtil.join(".", resultList);
    }

    public static List<String> permutation(List<String> resultList, List<String> sentenceList)
    {
        int index_buffer_x;
        int index_buffer_y;
        String sen_x;
        String sen_y;
        int length = resultList.size();
        // bubble sort derivative
        for (int i = 0; i < length; i++)
            for (int offset = 0; offset < length - i; offset++)
            {
                sen_x = resultList.get(i);
                sen_y = resultList.get(i + offset);
                index_buffer_x = sentenceList.indexOf(sen_x);
                index_buffer_y = sentenceList.indexOf(sen_y);
                // if the sentence order in sentenceList does not conform that is in resultList, reverse it
                if (index_buffer_x > index_buffer_y)
                {
                    resultList.set(i, sen_y);
                    resultList.set(i + offset, sen_x);
                }
            }

        return resultList;
    }

    public static List<String> pick_sentences(List<String> resultList, int max_length)
    {
        int length_counter = 0;
        int length_buffer;
        int length_jump;
        List<String> resultBuffer = new LinkedList<String>();
        for (int i = 0; i < resultList.size(); i++)
        {
            length_buffer = length_counter + resultList.get(i).length();
            if (length_buffer <= max_length)
            {
                resultBuffer.add(resultList.get(i));
                length_counter += resultList.get(i).length();
            }
            else if (i < (resultList.size() - 1))
            {
                length_jump = length_counter + resultList.get(i + 1).length();
                if (length_jump <= max_length)
                {
                    resultBuffer.add(resultList.get(i + 1));
                    length_counter += resultList.get(i + 1).length();
                    i++;
                }
            }
        }
        return resultBuffer;
    }



    public static void main(String[] args)
    {
        String content = "Мужчины реже уводят женщин, особенно если это женщины их друзей. И на это есть целый ряд причин, утверждают американские исследователи из университета Миссури."
                + "Учёные из Университета Миссури (США) решили разобраться, почему мужчины реже уводят женщин из семьи, чем женщины уводят мужчин. Оказывается, у мужчин резко падает уровень тестостерона — основного мужского полового гормона, когда они общаются с жёнами своих близких друзей. Однако такое явление наблюдается только у мужчин, минувших подростковый период, сообщают «Вести»."
                + "Как правило, здоровый взрослый мужчина воспринимает большинство женщин, не связанных с ним кровным родством, как потенциальных сексуальных партнёрш. Чтобы не запутаться в таком многообразии, инстинкты подсказывают выбирать ту, что произведёт здоровое потомство: отсюда мода на широкие бёдра (легко родит), пышную грудь (полноценно выкормит), румянец (признак здоровья и молодости). Профессор антропологии в университете Миссури и ведущий автор исследования Марк Флинн говорит: «Несмотря на то, что у мужчин есть масса шансов завоевать сердце жены друга, они ими не пользуются. Случаи, когда мужчина совершает такой поступок, чрезвычайно редки и, скорее, являются исключением, подтверждающим правило»."
                + "При этом друзья — для мужчин святое. Именно дружеские связи помогали людям строить сообщества, племена, а позднее, и целые государства. Мужчину, который посягнул на жену своего друга, считали во все времена предателем. Предательство в свою очередь порождало междоусобицу, в результате чего общество становилось уязвимым и нестабильным, и часто вовсе прекращало своё существование. Инстинкт отвращения к жёнам друзей был вызван необходимостью прекращения соперничества между мужчинами, полагают авторы нынешнего исследования. Если бы единственной целью каждого из нас было здоровое и многочисленное потомство, люди могли бы и вовсе не эволюционировать."
                + "Авторы работы, все подробности которого были опубликованы в журнале Human Nature, утверждают, что если правильно понять подобное поведение людей в обществе, можно будет решить массу проблем современности, таких как вооружённые конфликты и даже климатические изменения. Правда, учёные пока не сообщают, каким именно образом.";
        
        System.out.println(ArticleSummayExtractor.getTopSentenceList(content, 3));
    }
    
 
}
