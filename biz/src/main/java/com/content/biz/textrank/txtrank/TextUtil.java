package com.content.biz.textrank.txtrank;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class TextUtil {
    
    /**
     * 俄语分隔符
     */
    private static final String SPLIT_CHAR = "[ 、，。；？!,.;?!»«]";
    
    /** 
     * 作为关键字的最短长度，剔除小于2个字符的词 
     */
    private static final Integer SHORTEST_LENGTH = 2; 
    
    /**
     * 将文章分隔为字符串列表
     * @param content
     * @return
     */
    public static List<String> spiltWord(String content) {
        List<String> termList = new ArrayList<String>();
        String arr[] = content.split(SPLIT_CHAR);
        for (String str : arr) {
            termList.add(str);
        }
        return termList;
    }
    
    
   
    
    /**
     * 是否是停用词
     * @param t
     * @return
     */
    public static boolean shouldInclude(String t) {
        if (StopwordsVo.STOPWORDS_RU.contains(t)) {
            return false;
        }
        if (t.length() <= SHORTEST_LENGTH) {
            return false;
        }
        return true;
    }
    
    /**
     * 将文章分割为句子
     *
     * @param document
     * @return
     */
    public static List<String> spiltSentence(String document)
    {
        
        List<String> sentences = new ArrayList<String>();
        for (String line : document.split("[\r\n]"))
        {
            line = line.trim();
            if (line.length() == 0) continue;
            for (String sent : line.split("[.,.“”？?！!;;]"))
            {
                sent = sent.trim();
                if (sent.length() == 0) continue;
                sentences.add(sent);
            }
        }

        return sentences;
    }


    /**
     *　将stringCollection每个分段结尾添加字符串　delimiter
     * @param delimiter
     * @param stringCollection
     * @return
     */
    public static String join(String delimiter, Collection<String> stringCollection)
    {
        StringBuilder sb = new StringBuilder(stringCollection.size() * (16 + delimiter.length()));
        for (String str : stringCollection)
        {
            sb.append(str).append(delimiter);
        }

        return sb.toString();
    }

}
