package com.content.biz.textrank.txtrank;

public class StopwordsVo {
    /**
     * 俄语停用词
     */
    public static final String STOPWORDS_RU = "а,е,и,ж,м,о,на,не,ни,об,но,он,мне,мои,мож,она,они,"
       +"оно,мной,много,многочисленное,многочисленная,многочисленные,многочисленный,мною,мой,мог,могут,"
       + "можно,может,можхо,мор,моя,моё,мочь,над,нее,оба,нам,нем,нами,ними,мимо,немного,одной,одного,"
       + "менее,однажды,однако,меня,нему,меньше,ней,наверху,него,ниже,мало,надо,один,одиннадцать,одиннадцатый,"
       + "назад,наиболее,недавно,миллионов,недалеко,между,низко,меля,нельзя,нибудь,непрерывно,наконец,никогда,"
       + "никуда,нас,наш,нет,нею,неё,них,мира,наша,наше,наши,ничего,начала,нередко,несколько,обычно,опять,"
       + "около,мы,ну,нх,от,отовсюду,особенно,нужно,очень,отсюда,в,во,вон,вниз,внизу,вокруг,вот,восемнадцать,"
       + "восемнадцатый,восемь,восьмой,вверх,вам,вами,важное,важная,важные,важный,вдали,везде,ведь,вас,"
       + "ваш,ваша,ваше,ваши,впрочем,весь,вдруг,вы,все,второй,всем,всеми,времени,время,всему,всего,всегда,"
       + "всех,всею,всю,вся,всё,всюду,г,год,говорил,говорит,года,году,где,да,ее,за,из,ли,же,им,до,по,ими,"
       + "под,иногда,довольно,именно,долго,позже,более,должно,пожалуйста,значит,иметь,больше,пока,ему,имя,"
       + "пор,пора,потом,потому,после,почему,почти,посреди,ей,два,две,двенадцать,двенадцатый,двадцать,двадцатый,"
       + "двух,его,дел,или,без,день,занят,занята,занято,заняты,действительно,давно,девятнадцать,девятнадцатый,"
       + "девять,девятый,даже,алло,жизнь,далеко,близко,здесь,дальше,для,лет,зато,даром,первый,перед,затем,"
       + "зачем,лишь,десять,десятый,ею,её,их,бы,еще,при,был,про,процентов,против,просто,бывает,бывь,если,"
       + "люди,была,были,было,будем,будет,будете,будешь,прекрасно,буду,будь,будто,будут,ещё,пятнадцать,"
       + "пятнадцатый,друго,другое,другой,другие,другая,других,есть,пять,быть,лучше,пятый,к,ком,конечно,кому,"
       + "кого,когда,которой,которого,которая,которые,который,которых,кем,каждое,каждая,каждые,каждый,кажется,"
       + "как,какой,какая,кто,кроме,куда,кругом,с,т,у,я,та,те,уж,со,то,том,снова,тому,совсем,того,тогда,тоже,"
       + "собой,тобой,собою,тобою,сначала,только,уметь,тот,тою,хорошо,хотеть,хочешь,хоть,хотя,свое,свои,твой,"
       + "своей,своего,своих,свою,твоя,твоё,раз,уже,сам,там,тем,чем,сама,сами,теми,само,рано,самом,самому,самой,"
       + "самого,семнадцать,семнадцатый,самим,самими,самих,саму,семь,чему,раньше,сейчас,чего,сегодня,себе,тебе,"
       + "сеаой,человек,разве,теперь,себя,тебя,седьмой,спасибо,слишком,так,такое,такой,такие,также,такая,сих,"
       + "тех,чаще,четвертый,через,часто,шестой,шестнадцать,шестнадцатый,шесть,четыре,четырнадцать,четырнадцатый,"
       + "сколько,сказал,сказала,сказать,ту,ты,три,эта,эти,что,это,чтоб,этом,этому,этой,этого,чтобы,этот,стал,"
       + "туда,этим,этими,рядом,тринадцать,тринадцатый,этих,третий,тут,эту,суть,чуть,тысяч";
    
    /**
     * 英语停用词
     */
    public static final String STOPWORDS_EN = "able,about,above,according,accordingly,across,actually,after,"
        + "afterwards,again,against,ain't,allow,allows,almost,alone,along,already,also,although,always,among,"
        + "amongst,another,anybody,anyhow,anyone,anything,anyway,anyways,anywhere,apart,appear,appreciate,"
        + "appropriate,aren't,around,aside,asking,associated,available,away,awfully,became,because,become,becomes,"
        + "becoming,been,before,beforehand,behind,being,believe,below,beside,besides,best,better,between,beyond,"
        + "both,brief,c'mon,came,can't,cannot,cant,cause,causes,certain,certainly,changes,clearly,come,comes,"
        + "concerning,consequently,consider,considering,contain,containing,contains,corresponding,could,couldn't,"
        + "course,currently,definitely,described,despite,didn't,different,does,doesn't,doing,don't,done,down,"
        + "downwards,during,each,eight,either,else,elsewhere,enough,entirely,especially,even,ever,every,"
        + "everybody,everyone,everything,everywhere,exactly,example,except,fifth,first,five,followed,following,"
        + "follows,former,formerly,forth,four,from,further,furthermore,gets,getting,given,gives,goes,going,gone,"
        + "gotten,greetings,hadn't,happens,hardly,hasn't,have,haven't,having,hello,help,hence,here,here's,"
        + "hereafter,hereby,herein,hereupon,hers,herself,himself,hither,hopefully,howbeit,however,i'll,i've,"
        + "ignored,immediate,inasmuch,indeed,indicate,indicated,indicates,inner,insofar,instead,into,inward,"
        + "isn't,it'd,it'll,it's,itself,just,keep,keeps,kept,know,knows,known,last,lately,later,latter,latterly,"
        + "least,less,lest,let's,like,liked,likely,little,look,looking,looks,mainly,many,maybe,mean,meanwhile,"
        + "merely,might,more,moreover,most,mostly,much,must,myself,name,namely,near,nearly,necessary,need,needs,"
        + "neither,never,nevertheless,next,nine,nobody,none,noone,normally,nothing,novel,nowhere,obviously,often,"
        + "okay,once,ones,only,onto,other,others,otherwise,ought,ours,ourselves,outside,over,overall,particular,"
        + "particularly,perhaps,placed,please,plus,possible,presumably,probably,provides,quite,rather,really,"
        + "reasonably,regarding,regardless,regards,relatively,respectively,right,said,same,saying,says,second,"
        + "secondly,seeing,seem,seemed,seeming,seems,seenself,selves,sensible,sent,serious,seriously,seven,"
        + "several,shall,should,shouldn't,since,some,somebody,somehow,someone,something,sometime,sometimes,"
        + "somewhat,somewhere,soon,sorry,specified,specify,specifying,still,such,sure,take,taken,tell,tends,than,"
        + "thank,thanks,thanx,that,that's,thats,their,theirs,them,themselves,then,thence,there,there's,thereafter,"
        + "thereby,therefore,therein,theres,thereupon,these,they,they'd,they'll,they're,they've,think,third,this,"
        + "thorough,thoroughly,those,though,three,through,throughout,thru,thus,together,took,toward,towards,tried,"
        + "tries,truly,trying,twice,under,unfortunately,unless,unlikely,until,unto,upon,used,useful,uses,using,"
        + "usually,uucp,value,various,very,want,wants,wasn't,we'd,we'll,we're,we've,welcome,well,went,were,weren't,"
        + "what,what's,whatever,when,whence,whenever,where,where's,whereafter,whereas,whereby,wherein,whereupon,"
        + "wherever,whether,which,while,whither,who's,whoever,whole,whom,whose,will,willing,wish,with,within,"
        + "without,won't,wonder,would,would,wouldn't,you'd,you'll,you're,you've,your,yours,yourself,yourselves,zero";
}
