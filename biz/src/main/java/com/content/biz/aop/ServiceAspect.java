package com.content.biz.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ServiceAspect {

    private final Logger logger = LoggerFactory.getLogger(ServiceAspect.class);
    // 切入点表达式按需配置
    @Pointcut("execution(public * com.content.biz.aop.Aop*.*(..))")
    private void myPointcut() {
    }

    @Before("myPointcut()")
    public void before(JoinPoint joinPoint) {
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        logger.warn(className + "的" + methodName + "执行了");
        Object[] args = joinPoint.getArgs();
        StringBuilder log = new StringBuilder("入参为");
        for (Object arg : args) {
            log.append(arg + " ");
        }
        logger.warn(log.toString());
    }

    @AfterReturning(pointcut = "myPointcut()", returning = "returnVal")
    public void afterReturin(Object returnVal) {
        logger.warn("方法正常结束了,方法的返回值:" + returnVal);
    }

    @AfterThrowing(pointcut = "myPointcut()", throwing = "e")
    public void afterThrowing(Throwable e) {
        if (e!=null) {
            logger.error("通知中发现异常Exception", e);
        } else {
            logger.error("通知中发现未知异常", e);
        }
    }

    @Around("myPointcut()")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object result = null;
        long start=System.currentTimeMillis();
        result = proceedingJoinPoint.proceed();
        long endTime=System.currentTimeMillis();
        System.out.println("cost time:"+(endTime-start));
        return result;
    }
}