package com.content.biz.aop;

import org.aspectj.weaver.patterns.ExactAnnotationFieldTypePattern;
import org.springframework.stereotype.Component;

/**
 * Created by maoman on 16-11-28.
 */
@Component
public class AopService {
    public  int sum(int a,int b) throws Exception {
        if(a<0){
            throw new Exception("a < 0");
        }
        return a+b;
    }
}
