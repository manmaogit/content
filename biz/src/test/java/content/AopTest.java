package content;


import com.content.biz.aop.AopService;
import com.content.persitence.es.common.EsDaoSupport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class AopTest {

    @Resource
    public AopService aopTest;

    @Test
    public void AopTest(){
        try {
            aopTest.sum(-1,2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void esTest(){
        EsDaoSupport esDaoSupport=new EsDaoSupport();
        esDaoSupport.insertOneById("test_index","test_type","{\"test\":\"test\"}","123457");
    }


}
