package com.content.admin.controller;

import com.content.admin.util.*;
import com.content.admin.vo.entity.Article;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by maoman on 16-9-12.
 */
@Controller
public class ContentController {

    @RequestMapping(value = "article/list")
    public String contentList(HttpServletRequest request,Model model){

        int currentPage = PageUtils.parsePageNo(request.getParameter("currPage"));
        int pageSize = PageUtils.parsePageSize(request.getParameter("pageSize"));

        // 取当前时间，但是时，分，秒为０
        Calendar calendar = DateTimeUtil.getCurrentTimeOfDay();

        long currDate = calendar.getTimeInMillis();
        long startDate = QueryUtils.checkDate(request.getParameter("startDate"));
        long endDate = QueryUtils.checkDate(request.getParameter("endDate"));

        if (startDate == QueryUtils.INVALID_DATE) {
            // 对当前时间的10天前
            calendar.add(Calendar.DATE, -10);

            startDate = calendar.getTimeInMillis(); //
        }
        if (endDate == QueryUtils.INVALID_DATE) {
            endDate = currDate;// 当天
        }

        // 显示内容数据,显示一页
        Page<Article> page =new Page<Article>(1, currentPage,pageSize);

        List<Article> list=new ArrayList<>();
        for(int i=0;i<10;i++){
            Article article=new Article();
            article.setId(i);
            article.setCrawledTime(System.currentTimeMillis());
            article.setPubTime(System.currentTimeMillis());
            article.setTypeId(10);
            article.setSiteId(10);
            article.setSourceId(10);
            article.setTitle("测试标题");
            list.add(article);
        }

        page.setRecordList(list);
        model.addAttribute("page",page);
        model.addAttribute("startDate", QueryUtils.formatDate(new Date(startDate)));
        model.addAttribute("endDate", QueryUtils.formatDate(new Date(endDate)));

        return "article/list";
    }

    @RequestMapping(value = "article/listxy")
    public String contentListXy(HttpServletRequest request,Model model){

        int currentPage = PageUtils.parsePageNo(request.getParameter("currPage"));
        int pageSize = PageUtils.parsePageSize(request.getParameter("pageSize"));

        // 取当前时间，但是时，分，秒为０
        Calendar calendar = DateTimeUtil.getCurrentTimeOfDay();

        long currDate = calendar.getTimeInMillis();
        long startDate = QueryUtils.checkDate(request.getParameter("startDate"));
        long endDate = QueryUtils.checkDate(request.getParameter("endDate"));

        if (startDate == QueryUtils.INVALID_DATE) {
            // 对当前时间的10天前
            calendar.add(Calendar.DATE, -10);

            startDate = calendar.getTimeInMillis(); //
        }
        if (endDate == QueryUtils.INVALID_DATE) {
            endDate = currDate;// 当天
        }

        List<String> dateList=DateTimeUtil.getTimeList(startDate,endDate);

        List<String> cateList=new ArrayList<>();
        for(int i=0;i<10;i++){
            cateList.add("测试");
        }

        Map<String,Map<String,Long>> maps=new HashMap<String, Map<String, Long>>();
        for(String date:dateList){
            Map<String,Long> dayMap=new HashMap<>();
            for(String cate:cateList){
                dayMap.put(cate,10l);
            }
            maps.put(date,dayMap);
        }

        // 显示内容数据,显示一页
        Page<Map<String,Map<String,Long>>> page =new Page<Map<String,Map<String,Long>>>(1, currentPage,pageSize);


        page.setContent(maps);
        model.addAttribute("page",page);
        model.addAttribute("dateList",dateList);
        model.addAttribute("cateList",cateList);
        model.addAttribute("startDate", QueryUtils.formatDate(new Date(startDate)));
        model.addAttribute("endDate", QueryUtils.formatDate(new Date(endDate)));

        return "article/list_xy";
    }


    @RequestMapping(value = "article/chart")
    public String contentChart(HttpServletRequest request,Model model){

        Calendar currDate = DateTimeUtil.getCurrentTimeOfDay();
        long theDate = QueryUtils.checkDate(request.getParameter("theDate"));

        if (theDate == QueryUtils.INVALID_DATE) {
            theDate = currDate.getTimeInMillis();
        }else {
            currDate.setTimeInMillis(theDate);
        }

        currDate.add(Calendar.DATE, 1);
        long theDayAfter = currDate.getTimeInMillis();

        Map<String,Long>map=new HashMap<>();
        map.put("android",1l);
        map.put("ios",2l);
        map.put("other",3l);

        EchartsUtils.mapToChartFormat(map,model);

        model.addAttribute("theDate", QueryUtils.formatDate(new Date(theDate)));

        return "article/chart";
    }




}
