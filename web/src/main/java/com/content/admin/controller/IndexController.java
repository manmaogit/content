package com.content.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 主页的控制器 Created by machangsheng on 15/10/9.
 */
@Controller
public class IndexController {

    private static final Logger LOG = LoggerFactory.getLogger(IndexController.class);

    /**
     * 主界面
     * 
     * @return 结果集
     */
    @RequestMapping("/index")
    public String index() {

        return "index";
    }

    /**
     * 登录的展示页
     * 
     * @return 'login'
     */
    @RequestMapping("/login")
    public ModelAndView login(Model model) {
       // model.addAttribute("errorInfo", "");
        // return "login";

        ModelAndView view=new ModelAndView();
        view.addObject("errorInfo","");
        view.setViewName("login");
        return  view;
    }
    
  

    /**
     * 处理登录请求,其实这个更好的是一个ajax过程
     * 
     * @param username username
     * @param password password
     * @param model model
     * @return index or login page
     */
    @RequestMapping(value = "/doLogin", method = RequestMethod.POST)
    public String doLogin(String username, String password, Model model, HttpSession session) {
        String errorInfo;
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            errorInfo = "用户名或密码未输入!";
        } else {
            // 登录验证处理
            // 1. 生成token
            if ("admin".equals(username) && "Aa123456".equals(password)) {
                session.setAttribute("userInfo", username);
                return "redirect:/index";
            } else {
                errorInfo = "用户名/密码错误!";
            }
        }

        model.addAttribute("errorInfo", errorInfo);
        return "login";
    }

    /**
     * 退出操作,移除当前session中的内容,并转到登录页
     * 
     * @param request request
     * @return 返回页面
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request) {
        request.getSession().removeAttribute("userInfo");
        return "redirect:/login";
    }


    /**
     * 无权限访问, 需要展示的页面
     * 
     * @return 无权限页面
     */
    @RequestMapping(value = "/unauthorized")
    public String unauthorized() {
        return "/unauthorized";
    }

    /**
     * 404请求
     * 
     * @return 404页面
     */
    @RequestMapping(value = "/errors/err404")
    public String err404(Model model) {
        model.addAttribute("title", "资源未找到");
        model.addAttribute("message", "对不起, 未找到您请求的资源,请检查您的输入是否有误!");
        return "/errors/error";
    }

    /**
     * 内部错误, 500
     * 
     * @return 500页面
     */
    @RequestMapping(value = "/errors/err500")
    public String err500(Model model) {
        model.addAttribute("title", "系统繁忙");
        model.addAttribute("message", "对不起, 系统繁忙, 请联系管理员!");
        return "/errors/error";
    }

    /**
     * 服务不可用, 503
     * 
     * @return 503页面
     */
    @RequestMapping(value = "/errors/err503")
    public String err503(Model model) {
        model.addAttribute("title", "服务不可用");
        model.addAttribute("message", "对不起, 您请求的资源暂时不可用, 请联系管理员!");
        return "/errors/error";
    }
    
 

}
