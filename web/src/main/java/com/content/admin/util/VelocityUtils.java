package com.content.admin.util;
//package com.tcl.iread.system.util;
//
//import com.yc.newoa.search.domain.MailTemplate;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.velocity.VelocityContext;
//import org.apache.velocity.app.VelocityEngine;
//
//import java.io.StringWriter;
//import java.util.Map;
//import java.util.Properties;
//
///**
// * VelocityUtil
// * 这个没有什么参数,所以做成了一个单例的形式
// * 本来也可以和Email集成,但考虑到可能以后还会有其它模块用到,所以就先不集成了
// * 独立出来
// * Created by machangsheng on 15/10/23.
// */
//public class VelocityUtils {
//    /**
//    * 引擎
//    */
//    private VelocityEngine velocityEngine;
//    /**
//     * 流程实例, 加上volatile关键词以禁止指令重排序
//     */
//    private static volatile VelocityUtils instance = null;
//    /**
//     * 用于同步的对象
//     */
//    private static final Object sync = new Object();
//
//    private VelocityUtils(){
//        //初始化参数
//        Properties properties=new Properties();
//        //设置velocity资源加载方式为class
//        properties.setProperty("resource.loader", "class");
//        //设置velocity资源加载方式为class时的处理类
//        properties.setProperty("class.resource.loader.class",
//                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
//        //实例化一个VelocityEngine对象
//        velocityEngine=new VelocityEngine(properties);
//    }
//
//    /**
//     * 以单例方式来创建一个实例
//     * @return 实例
//     */
//    public static VelocityUtils getInstance(){
//        if(instance == null) {
//            synchronized(sync){
//                //到这里再判断一次,这个必须要加上,否则同步的意义就不大了
//                if(instance == null) {
//                    instance = new VelocityUtils();
//                }
//            }
//        }
//        return instance;
//    }
//
//    /**
//     * 加载模板文件, 并用参数填充后, 得到转换后的字符内容
//     * @param vmFile 模板文件
//     * @param infoMap 参数
//     * @return 结果
//     */
//    public String loaderFromCLass(String vmFile,
//        Map<String, Object> infoMap) {
//        //实例化一个VelocityContext
//        VelocityContext context=new VelocityContext(infoMap);
//        //实例化一个StringWriter
//        StringWriter writer=new StringWriter();
//        velocityEngine.mergeTemplate(vmFile,"UTF-8", context, writer);
//
//        return writer.toString();
//    }
//
//    /**
//     * 直接读取字符串的内容
//     * @param templateContent 模板内容
//     * @param infoMap 参数
//     * @return 结果
//     */
//    public String loaderFromString(String templateContent, Map<String, Object> infoMap){
//            //实例化一个VelocityContext
//            VelocityContext context=new VelocityContext(infoMap);
//            //实例化一个StringWriter
//            StringWriter writer=new StringWriter();
//            velocityEngine.evaluate(context, writer, "velocity.log", templateContent);
//
//            return writer.toString();
//    }
//
//    /**
//     * 根据邮件模板中填写的内容,还决定是使用字符串还是文件
//     * @param mailTemplate 模板
//     * @param infoMap 参数
//     * @return 解析后的结果
//     */
//    public String loaderFromMT(MailTemplate mailTemplate, Map<String, Object> infoMap){
//        if(mailTemplate == null ) return null;
//
//        boolean pathEmpty = StringUtils.isBlank(mailTemplate.getPath());
//        boolean contentEmpty = StringUtils.isBlank(mailTemplate.getContent());
//
//        if(contentEmpty && pathEmpty){
//            return null;
//        }
//
//        return  contentEmpty ? loaderFromCLass(mailTemplate.getPath(), infoMap)
//                : loaderFromString(mailTemplate.getContent(), infoMap);
//    }
//}
