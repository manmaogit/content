package com.content.admin.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.ui.Model;

/**
 * 
 * @author yangwang
 *
 */
public class EchartsUtils {
	/**
     * @author yangwang
     * map转换为前端Echartt显示的格式，并添加的Model中
     * @param map
     * @param model 模型
     */
    public static void mapToChartFormat(Map<String, Long> map, Model model){
    	StringBuilder keys = new StringBuilder();
    	StringBuilder values = new StringBuilder();
    	List<String> keyList = new ArrayList<String>();
    	keyList.addAll(map.keySet());
    	Collections.sort(keyList);
    	for(String s : keyList){
    		keys.append(s).append(",");
    		values.append(map.get(s) == null ? 0 : map.get(s)).append(",");
    	}
    	keys.deleteCharAt(keys.length() - 1);
    	values.deleteCharAt(values.length() - 1);
    	model.addAttribute("keys", keys);
    	model.addAttribute("values", values);
    }
}
