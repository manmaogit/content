package com.content.admin.util;

/**
 * 分页辅助工具类 Created by machangsheng on 15/10/9.
 */
public class PageUtils {

    // private static Log
    /**
     * 获取页码,保证返回一个有效的页码
     * 
     * @param pageNo 页码
     * @return 有效页码
     */
    public static Integer parsePageNo(String pageNo) {
        return TypeUtils.parseInt(pageNo, FIRSTPAGE);
    }

    /**
     * 获取每页条数,保证返回一个有效的条数
     * 
     * @param pageSizeStr 每页记录数
     * @return 每页记录数
     */
    public static Integer parsePageSize(String pageSizeStr) {
        return TypeUtils.parseInt(pageSizeStr, PAGESIZE);
    }

    /**
     * 热词个数,保证返回一个有效的条数
     * 
     * @param topSizeStr 热词个数
     * @return 转化后的热词个数
     */
    public static Integer parseTopSize(String topSizeStr) {

        return TypeUtils.parseInt(topSizeStr, MINTOPSIZE);
    }

    /**
     * 默认分页大小
     */
    public static final int PAGESIZE = 20;
    /**
     * 第一页
     */
    public static final int FIRSTPAGE = 1;
    /**
     * 热词榜显示默认数量
     */
    public static final int MINTOPSIZE = 10;
}
