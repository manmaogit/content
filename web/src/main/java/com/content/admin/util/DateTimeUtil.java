package com.content.admin.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateTimeUtil {

    private final static Logger LOG = LoggerFactory.getLogger(DateTimeUtil.class);

    private static final DateTimeFormatter dateFormatter =
        DateTimeFormat.forPattern("yyyy-MM-dd");

    private static final DateTimeFormatter utcDateFormatter =
        DateTimeFormat.forPattern("yyyy-MM-dd").withZone(DateTimeZone.UTC);

    public static long getCurrentMillsOfUTC() {
        DateTime dateTime = new DateTime(new Date());
        DateTime utcDateTime = dateTime.withZone(DateTimeZone.UTC);
        return utcDateTime.getMillis();
    }

    public static int getCurrentYear() {
        DateTime dateTime = new DateTime(new Date());
        DateTime utcDateTime = dateTime.withZone(DateTimeZone.UTC);
        return utcDateTime.getYear();
    }

    public static int getCurrentMonth() {
        DateTime dateTime = new DateTime(new Date());
        DateTime utcDateTime = dateTime.withZone(DateTimeZone.UTC);
        return utcDateTime.getMonthOfYear();
    }

    public static int getCurrentDay() {
        DateTime dateTime = new DateTime(new Date());
        DateTime utcDateTime = dateTime.withZone(DateTimeZone.UTC);
        return utcDateTime.getDayOfMonth();
    }

    public static Calendar getCurrentTimeOfDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar;
    }

    public static DateTime fromDateString(String date) {
        return dateFormatter.parseDateTime(date);
    }

    /**
     * 时间转换为字符串
     *
     * @param format ：格式方式
     * @param date   ：时间
     * @return
     */
    public static String fomartDateToStr(String format, Date date) {
        String dateString = null;
        try {
            if (null != date) {
                SimpleDateFormat formatter = new SimpleDateFormat(format);
                dateString = formatter.format(date);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return dateString;
    }

    /**
     * 获取UTC时间
     *
     * @param millis
     * @return
     */
    public static String toUtcTimeString(Long millis) {
        return utcDateFormatter.print(millis);
    }
    
    
    /**
     * 获取时间列表
     * @param startDate
     * @param endDate
     * @return
     */
    public static List<String> getTimeList(Long startDate, Long endDate){
        
        List<String>dateList=new ArrayList<String>();
        Calendar cal = Calendar.getInstance();
        long sd = startDate;
        long ed = endDate;
        // 遍历
        while (sd <= endDate) {
            cal.setTimeInMillis(sd);
            cal.add(Calendar.DATE, 1);
            ed = cal.getTimeInMillis();
            
            String timeStr = DateTimeUtil.toUtcTimeString(ed);
            dateList.add(timeStr);
            sd = ed;
            
            if (sd > endDate) 
                break;
        }
        
        return dateList;
    } 
    
    /**
     * 日期每天加1
     * @author panzhu
     * @param startDate
     * @param endDate
     * @return list
    */
    public static List<String> DayPulsOne(long startDate,long endDate){
        List<String> list =new ArrayList<String>();
        for(long i=startDate;i<=endDate;){
            DateTime dateTime = new DateTime(i);
            list.add(dateTime.toString("yyyy-MM-dd"));
            i = dateTime.plusDays(1).getMillis();
        }
        return list;
    }
}
