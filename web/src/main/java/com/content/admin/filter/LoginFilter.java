package com.content.admin.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 登录验证的Filter Created by csma on 7/19/16.
 */
public class LoginFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(LoginFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        // 打印请求路径
        String reqUri = httpServletRequest.getRequestURI();
        String qs =
                httpServletRequest.getQueryString() == null ? "" : httpServletRequest
                        .getQueryString();
        LOG.info("{} {} {} {}", httpServletRequest.getRemoteAddr(), httpServletRequest.getMethod(),
                reqUri, qs);

        String path = httpServletRequest.getServletPath();
        
        if (!path.startsWith("/admin")) {
            // 如果不是以admin开头的，则不处理
            chain.doFilter(request, response);
        } else {

            // 检查是否已经登录
            Object userInfo = httpServletRequest.getSession().getAttribute("userInfo");

            if (userInfo == null) {
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/login");
            } else {
                // 请求成功，这个应该继续
                chain.doFilter(request, response);
            }
        }
    }

    @Override
    public void destroy() {

    }
}
