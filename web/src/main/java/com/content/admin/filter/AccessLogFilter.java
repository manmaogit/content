package com.content.admin.filter;

import com.common.util.crypto.AesUtil;

import com.content.admin.vo.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AccessLogFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(AccessLogFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String reqUri = req.getRequestURI();

        String qs = req.getQueryString() == null ? "" : req.getQueryString();
        String ct = req.getContentType() == null ? "" : req.getContentType();

        LOG.info("{} {} {} {} {}", req.getRemoteAddr(), req.getMethod(), reqUri, qs, ct);

    /*    // 当请求地址不是请求登录URL时
        if (reqUri.startsWith("/web/") && !reqUri.equals("/web/login")) {

            Integer operatorId = getOpIdFromCookie(req);

            if (operatorId == null) { // 数据为空
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().print("{\"code\":401,\"msg\":\"请登录\"}");
                return;
            }

            request.setAttribute(Constants.ATTR_OPERATOR_ID, operatorId);
            updateLoginCookie(response, operatorId); // 更新operatorId

        }*/

        chain.doFilter(request, response);
    }

    private Integer getOpIdFromCookie(HttpServletRequest request) {
        String value = null;
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(Constants.LOGIN_COOKIE_NAME)) {
                    value = cookie.getValue();
                    break;
                }
            }
        }

        if (value == null) {
            return null;
        }

        String s = AesUtil.decrypt(value, Constants.AES_KEY);
        if (s != null) {
            int i = s.indexOf('_');
            if (i > 0) {
                long t = Long.parseLong(s.substring(i + 1));// 获取时间戳
                if (System.currentTimeMillis() - t < Constants.LOGIN_COOKIE_TIMEOUT) {
                    return new Integer(s.substring(0, i));
                }
            }
        }

        return null;
    }

    /**
     * 
     * cookies 的 (id_time)
     * 
     * @param response
     * @param operatorId
     */
    private void updateLoginCookie(ServletResponse response, Integer operatorId) {
        String text = String.valueOf(operatorId) + "_" + System.currentTimeMillis();
        String s = AesUtil.encrypt(text, Constants.AES_KEY);
        if (s != null) {
            Cookie ck = new Cookie(Constants.LOGIN_COOKIE_NAME, s);
            ck.setPath("/");
            ck.setMaxAge(-1);
            ck.setHttpOnly(true);
            ((HttpServletResponse) response).addCookie(ck);
        }
    }

    @Override
    public void destroy() {

    }

}
