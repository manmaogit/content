package com.content.admin.vo;

public class Constants {
    /**
     * 值：{operatorId}_{最后操作时间戳} AES密文
     */
    public static final String LOGIN_COOKIE_NAME = "lop";

    /**
     * 登录信息Cookie过期时间 30分钟，单位毫秒
     */
    public static final long LOGIN_COOKIE_TIMEOUT = 30L * 60 * 1000;

    /**
     * AES加密文件算法Key
     */
    public static final String AES_KEY = "poiqewuroiqweroiewrnoewrer";

    /**
     * 属性：operator id，存放于request
     */
    public static final String ATTR_OPERATOR_ID = "operator_id";
}
