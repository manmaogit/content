<link href="../styles/bootstrap.min.css?v=2.5" rel="stylesheet" type="text/css" />
<link href="../styles/bootstrapValidator.min.css?v=1.0" rel="stylesheet" type="text/css" />
<!-- MetisMenu CSS -->
<link href="../styles/metisMenu.min.css" rel="stylesheet" type="text/css" />
<!-- Timeline CSS -->
<link href="../styles/timeline.css" rel="stylesheet" type="text/css" />
<!-- Custom CSS -->
<link href="../styles/sb-admin-2.css" rel="stylesheet" type="text/css" />
<!-- Morris Charts CSS -->
<link href="../styles/morris.css" rel="stylesheet" />
<!-- Custom Fonts -->
<link href="../styles/font-awesome.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    div.dataTables_paginate {
        margin: 0;
        white-space: nowrap;
        text-align: right;
    }

    div.dataTables_paginate ul.pagination {
        margin: 2px 0;
        white-space: nowrap;
    }
</style>
