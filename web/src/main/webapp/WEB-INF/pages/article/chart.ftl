<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<#import "/spring.ftl" as spring/>
<!--分页组件-->
<#import "../macro/page.ftl" as mypage />
<#import  "../macro/taglib.ftl" as tag />

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>爬虫统计管理平台</title>
    <!-- Bootstrap Core CSS -->
    <#include "../include/header.ftl" />
    <link href="${contextPath}/styles/datepicker.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        a, span {
            margin-left: 10px;
        }

        a.currRank {
            font-weight:bold;
            color:blue
        }
    </style>
</head>

<body>

<div id="wrapper">

    <#include "../include/navbar.ftl" />

    <div id="page-wrapper">
        <!-- here is the content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">已发布文章数据统计</h1>
            </div>
            <!-- /.col-lg-12 -->

            <!-- table info begin -->
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <form class="form-horizontal  container-fluid one-line-form"
                                      id="addForm" name="addForm" action="${contextPath}/article/chart">
                                    <!-- row one -->
                                    <div class="row">
                                        <div class="form-group">

                                            <label class="col-md-5 control-label">统计日期</label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" id="theDate"
                                                       name="theDate" readonly value="${theDate}" />
                                            </div>
                                            <div class="col-md-4">
                                                <input type="submit" class="btn btn-primary" value="统计"
                                                       style="margin-right: 10px" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <span id="keysSpan" class="hide">${keys}</span>
                                <span id="timesSpan" class="hide">${values}</span>
                                <div id="main" style="width: 950px;height: 500px"></div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- table info end -->
        </div>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<#include "../include/footer.ftl" />
<!-- Bootstrap Core JavaScript -->
<script src="${contextPath}/script/bootstrap/bootstrap-datepicker.js"></script>
<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#theDate").datepicker({
            format: 'yyyy-mm-dd'
            //format:'yyyy-mm-dd hh:ii:ss',最小单位到秒
        });
    });

    //do it
</script>
<script type="text/javascript">

    var arrStrTimes = $("#timesSpan").text().split(",");
    var arrNumTimes = new Array();
    for(var k = 0; k < arrStrTimes.length; k++){
        arrNumTimes.push(Number(arrStrTimes[k]));
    }
    // 路径配置
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });

    // 使用
    require(
            [
                'echarts',
                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
                'echarts/chart/line' // 使用拆线图
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main'));

                var option = {
                    //title
                    title : {
                        text: $('#theDate').val() + '分维度统计'
                    },
                    tooltip: {
                        show: true,
                        trigger: 'axis'
                    },
                    legend: {
                        data:['数量'] //图例内容数组
                    },
                    //工具栏
                    toolbox: {
                        show : true,
                        feature : {
                            dataView : {show: true, readOnly: true},
                            magicType : {show: true, type: ['line', 'bar']},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    calculable : true,//启用拖放重计算
                    xAxis : [
                        {
                            type : 'category',
                            data : $("#keysSpan").text().split(",")
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : [
                        {
                            name:'发布数量',
                            type:'line',
                            data:arrNumTimes,
                            markPoint : {
                                data : [
                                    {type : 'max', name: '当日最高'},
                                    {type : 'min', name: '当日最低'}
                                ]
                            },
                            markLine : {
                                data : [
                                    {type : 'average', name: '当日平均'}
                                ]
                            }
                        }
                    ]
                };

                // 为echarts对象加载数据
                myChart.setOption(option);
            }
    );

    /**
     * 导出excel
     */
    function exportData(){
        var theDate = $("#theDate").val();
        window.location.href = "exportHourlySearch?theDate=" + theDate + "&dataType=" + $("#dataType").val();
    }
</script>

</body>

</html>
