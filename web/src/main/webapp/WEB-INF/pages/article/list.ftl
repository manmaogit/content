<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<#import "/spring.ftl" as spring/>
<!--分页组件-->
<#import "../macro/page.ftl" as mypage />
<#import "../macro/taglib.ftl" as tag />

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>管理平台</title>
    <!-- Bootstrap Core CSS -->
    <#include "../include/header.ftl" />
    <link href="${contextPath}/styles/datepicker.css" rel="stylesheet" type="text/css" />
</head>


<body>
<div id="wrapper">
    <#include "../include/navbar.ftl" />
    <#setting datetime_format="yyyy-MM-dd HH:mm:ss.SSS"/>
    <div id="page-wrapper">
        <!-- here is the content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">统计</h1>
            </div>
            <!-- /.col-lg-12 -->

            <!-- table info begin -->
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">明细记录 <a href="chart">查看统计图</a></div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <form class="form-horizontal  container-fluid one-line-form"
                                      id="addForm" name="addForm" action="${contextPath}/article/list">
                                    <!-- row one -->
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">起始日期</label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" id="startDate"
                                                       name="startDate" readonly value="${startDate}" />

                                            </div>
                                            <label class="col-md-2 control-label">结束日期</label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" id="endDate"
                                                       name="endDate" readonly value="${endDate}" />
                                            </div>
                                            <div class="col-md-4">
                                                <input type="submit" class="btn btn-primary" value="查询"
                                                       style="margin-right: 10px" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>文章ID</th>
                                            <th>标题</th>
                                            <th>类型</th>
                                            <th>抓取时间</th>
                                            <th>发布时间</th>
                                            <th>二级源ID</th>
                                            <th>一级源编码</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <#list page.recordList as article>
                                            <tr>
                                                <td>${article.id}</td>
                                                <td>${article.title}</td>
                                                <td>${article.typeId}</td>
                                                <td>${(article.crawledTime?number_to_datetime)!"无"}</td>
                                                <td>${article.pubTime?number_to_datetime}</td>
                                                <td>${article.sourceId}</td>
                                                <td>${article.siteId}</td>
                                            </tr>
                                        </#list>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive ?number_to_datetime-->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <@mypage.page pageItem=page param="startDate=${startDate}&endDate=${endDate}&" url="${contextPath}/article/list" />
            <!-- table info end -->
        </div>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<#include "../include/footer.ftl" />
<!-- Bootstrap Core JavaScript -->
<script src="${contextPath}/script/bootstrap/bootstrap-datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#startDate").datepicker({
            format: 'yyyy-mm-dd'
            //format:'yyyy-mm-dd hh:ii:ss',最小单位到秒
        });

        $("#endDate").datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>
</body>

</html>
