package com.content.batch.redislock;

import javax.annotation.Resource;

import com.common.util.StringUtil;
import com.content.biz.redis.RedisManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 抽象分布式Job（自动获取REDIS分布式锁）,子类实现process()方法进行业务处理
 */
public abstract class AbstractDistributedJob {

    @Resource
    private RedisManager redisManager;

    protected final Logger LOG = LoggerFactory.getLogger(getClass());

    public void run() {
        LOG.info("run job...");
        Long st = System.currentTimeMillis();
        
        String lockKey = "project:" + getClass().getSimpleName() + ":lock";
        String lockValueStr = redisManager.getAndSet(lockKey, st.toString());
        
        Long lockValue= StringUtil.parserToLong(lockValueStr, 0l);
        
        // 取得执行锁：未运行过，或距上次运行时间超过2分钟
        boolean locked = lockValue == null || st - lockValue > 120000;
        if (!locked) {
            LOG.info("can not get lock, exit job.");
            return;
        }
        
        try {
            process();
        } catch (Exception e) {
            LOG.error("job error", e);
        } finally {
            long cost = System.currentTimeMillis() - st;
            LOG.info("job finished, cost {} ms.", cost);
        }
    }

    public abstract void process() throws Exception;
}