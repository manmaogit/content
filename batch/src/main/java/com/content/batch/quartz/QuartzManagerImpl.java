package com.content.batch.quartz;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Service;

/**
 * Quartz定时任务调度器管理类
 *
 * @author deng.zhang
 * @since 1.0.0 Created Time: 2014-10-29 14:43
 */
@Service
public class QuartzManagerImpl implements QuartzManager {
  /**
   * 任务组名
   */
  private static final String JOB_GROUP_NAME = "PUSH_MESSAGE_JOB_GROUP";
  /**
   * 触发器组名
   */
  private static final String TRIGGER_GROUP_NAME = "PUSH_MESSAGE_TRIGGER_GROUP";
  /**
   * Quartz调度器
   */
  private Scheduler scheduler;

  /**
   * 初始化调度器
   *
   * @throws SchedulerException
   */
  @PostConstruct
  public void init() throws SchedulerException {
    SchedulerFactory schedulerFactory = new StdSchedulerFactory();
    scheduler = schedulerFactory.getScheduler();
  }

  /**
   * 添加一个定时任务，使用默认的任务组名、触发器组名，触发器的名称跟任务的一样
   *
   * @param jobName 任务名称
   * @param cls 任务
   * @param time 任务执行的时间,Cron表达式
   */
  @SuppressWarnings("rawtypes")
  @Override
  public void addJob(String jobName, Class cls, String time) {
    try {
      JobDetail jobDetail = new JobDetail(jobName, JOB_GROUP_NAME, cls);
      CronTrigger trigger = new CronTrigger(jobName, TRIGGER_GROUP_NAME);
      trigger.setCronExpression(time);
      scheduler.scheduleJob(jobDetail, trigger);
      if (!scheduler.isShutdown()) {
        scheduler.start();
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * 移除一个定时任务 使用默认的任务组名、触发器组名，触发器名跟任务名一致
   *
   * @param jobName 任务名称
   */
  @Override
  public void removeJob(String jobName) {
    try {
      scheduler.pauseTrigger(jobName, TRIGGER_GROUP_NAME);// 暂停触发器
      scheduler.unscheduleJob(jobName, TRIGGER_GROUP_NAME);// 移除触发器
      scheduler.deleteJob(jobName, JOB_GROUP_NAME);// 删除任务
    } catch (SchedulerException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * 启动所有定时任务
   */
  @Override
  public void startJobs() {
    try {
      scheduler.start();
    } catch (SchedulerException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * 关闭所有定时任务
   */
  @Override
  public void shutdownJobs() {
    try {
      if (!scheduler.isShutdown()) {
        scheduler.shutdown();
      }
    } catch (SchedulerException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * 当关闭应用时，停止所有调度任务,并关掉调度器
   */
  @PreDestroy
  public void stopQuartz() {
    try {
      if (!scheduler.isShutdown()) {
        scheduler.shutdown();
      }
    } catch (SchedulerException e) {
      throw new RuntimeException(e);
    }
  }
}
