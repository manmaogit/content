package com.content.batch.quartz;

/**
 * Quartz定时任务调度器管理类
 *
 * @author deng.zhang
 * @since 1.0.0 Created Time: 2014-10-29 14:39
 */
public interface QuartzManager {

  /**
   * 启动所有定时任务
   */
  void startJobs();

  /**
   * 添加一个定时任务，使用默认的任务组名、触发器组名，触发器的名称跟任务的一样
   *
   * @param jobName 任务名称
   * @param cls 任务
   * @param time 任务执行的时间,Cron表达式
   */
  @SuppressWarnings("rawtypes")
  void addJob(String jobName, Class cls, String time);

  /**
   * 移除一个定时任务 使用默认的任务组名、触发器组名，触发器名跟任务名一致
   *
   * @param jobName 任务名称
   */
  void removeJob(String jobName);

  /**
   * 关闭所有定时任务
   */
  void shutdownJobs();
}
