package com.common.exception;
/**
 * 用户自定义异常
 * 
 * @author maoman
 *
 */
public class ParamException extends RuntimeException {
    private static final long serialVersionUID = -3094498521448574316L;
    
    public ParamException(String message){
        super(message);
    }
}
