package com.common.exception;

/************
 * 
 * 用户自定义异常
 * 
 * @author maoman
 *
 *
 */

public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 1397410891768947160L;
    
    public static final int INVALID_AUTH = 401;
    public static final int INTERNAL_ERROR = 500;
    
    private int code;
    
    public ServiceException(int code, String message) {
        super(message);
        this.code = code;
    }
    
    public ServiceException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
    
    public int getCode() {
        return code;
    }
}
