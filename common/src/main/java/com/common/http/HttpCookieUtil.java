package com.common.http;

import com.common.exception.ServiceException;
import org.springframework.util.Base64Utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class HttpCookieUtil {
    
    /**
     * 获取HttpCookies值
     *   <p>根据key获取cookies<br>
     *   
     * @param key  
     * @param request
     * @return value
     */
    public static String getCookie(String key,HttpServletRequest request){
        
        Cookie cookies[]=request.getCookies();
        for(Cookie cookie:cookies){
            if(cookie.getName().equals(key)){
                    return cookie.getValue();
             }
        }
        return null;
    }
    
    /**
     *  压缩Cookie
     *  
     *  @param c
     *  @param response
     */
    public static void compressCookie(Cookie c,HttpServletResponse res){
        try {
            ByteArrayOutputStream bos=new ByteArrayOutputStream();
            DeflaterOutputStream dos=new DeflaterOutputStream(bos);
            dos.write(c.getValue().getBytes()); //对
            dos.close();
            String compress=Base64Utils.encodeToString(bos.toByteArray());
            
            res.addCookie(new Cookie(c.getName(), compress));

        } catch (IOException e) {
            //手动抛出异常操作
            // new throw
            throw new ServiceException(1003, "compress cookie error");
        }
    }
    
    /**
     * 解压Cookie
     * 
     * @param c
     */
    public static void unCompress(Cookie c){
        try {
                ByteArrayOutputStream out=new ByteArrayOutputStream();
                byte[] compress = Base64Utils.decodeFromString(new String(c.getValue()));
                ByteArrayInputStream bis=new ByteArrayInputStream(compress);
                InflaterInputStream inflater=new InflaterInputStream(bis);
            
                byte[] b=new byte[1024];
                int count;
        
                while((count = inflater.read(b)) >= 0){
                        out.write(b,0,count);
                }
                inflater.close();
                
                c.setValue(new String( out.toByteArray()));
                
            }catch (IOException e) {
               
            }
    }
    
    
}
