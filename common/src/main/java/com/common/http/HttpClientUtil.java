package com.common.http;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.*;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.net.UnknownHostException;
import java.nio.charset.CodingErrorAction;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
public class HttpClientUtil {
    
    private final static Logger LOG = LogManager.getLogger("errorlog");
    
    private static CloseableHttpClient httpClient = null;
    private static RequestConfig requestConfig = null;
    
    private final static int socketTimeout = 10000;
    private final static int connectTimeout = 30000;
    
    static {
        try {
            
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            SSLContext sslContext =
                    SSLContexts.custom().useTLS().loadTrustMaterial(trustStore).build();
            
            sslContext.init(null, new TrustManager[] {new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }
                public void checkServerTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                
            }}, null);
            SSLConnectionSocketFactory sslSFactory =
                    new SSLConnectionSocketFactory(sslContext,
                            SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            Registry<ConnectionSocketFactory> socketFactoryRegistry =
                    RegistryBuilder.<ConnectionSocketFactory>create()
                            .register("http", PlainConnectionSocketFactory.INSTANCE)
                            .register("https", sslSFactory).build();
            
            PoolingHttpClientConnectionManager connManager =
                    new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            SocketConfig socketConfig = SocketConfig.custom().setTcpNoDelay(true).build();
            connManager.setDefaultSocketConfig(socketConfig);
            ConnectionConfig connectionConfig =
                    ConnectionConfig.custom().setMalformedInputAction(CodingErrorAction.IGNORE)
                            .setUnmappableInputAction(CodingErrorAction.IGNORE)
                            .setCharset(Consts.UTF_8).build();
            connManager.setDefaultConnectionConfig(connectionConfig);
            connManager.setMaxTotal(200);
            connManager.setDefaultMaxPerRoute(20);
            HttpRequestRetryHandler retryHandler = new HttpRequestRetryHandler() {
                public boolean retryRequest(IOException exception, int executionCount,
                        HttpContext context) {
                    if (executionCount >= 3) {
                        return false;
                    }
                    if (exception instanceof InterruptedIOException) {
                        return true;
                    }
                    if (exception instanceof ConnectTimeoutException) {
                        return true;
                    }
                    if (exception instanceof UnknownHostException) {
                        return false;
                    }
                    if (exception instanceof SSLException) {
                        return false;
                    }
                    HttpRequest request = HttpClientContext.adapt(context).getRequest();
                    if (!(request instanceof HttpEntityEnclosingRequest)) {
                        return true;
                    }
                    return false;
                }
            };
            httpClient =
                    HttpClients.custom().setConnectionManager(connManager)
                            .setRetryHandler(retryHandler).build();
            
            requestConfig =
                    RequestConfig.custom().setSocketTimeout(socketTimeout)
                            .setConnectTimeout(connectTimeout).build();
        } catch (Exception e) {
            LOG.error("create httpclient failed", e);
        }
    }
    public static String get(String url) throws IOException {
        if (httpClient == null) {
            throw new IllegalStateException("httpClient is null");
        }
        HttpGet httpGet = new HttpGet(url);
        httpGet.setConfig(requestConfig);
        CloseableHttpResponse response = null;
        String result = null;
        
        LOG.info("get, url: {}", url);
        try {
            response = httpClient.execute(httpGet);
            
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity);
                LOG.info("get finished, return: \n{}", result);
            }
            
        } catch (IOException e) {
            LOG.error("get error, url: {}", url);
            throw e;
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
    public static File getFile(String url, String filePath) throws IOException {
        if (httpClient == null) {
            throw new IllegalStateException("httpClient is null");
        }
        HttpGet httpGet = new HttpGet(url);
        httpGet.setConfig(requestConfig);
        CloseableHttpResponse response = null;
        File file = new File(filePath);
        LOG.info("get file, url: {}", url);
        try {
            response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream is = null;
                Header h = response.getFirstHeader("Content-Encoding");
                if (h != null && h.getValue() != null
                        && h.getValue().toLowerCase().contains("gzip")) {
                    is = new GZIPInputStream(entity.getContent());
                } else {
                    is = entity.getContent();
                }
                BufferedInputStream bis = new BufferedInputStream(is);
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                byte[] b = new byte[1024];
                int len = 0;
                while ((len = bis.read(b)) != -1) {
                    bos.write(b, 0, len);
                }
                bos.flush();
                bis.close();
                bos.close();
                LOG.info("get file finished");
            }
        } catch (IOException e) {
            LOG.error("getFile error, url: {}", url);
            throw e;
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }
    public static File getFileWithRandName(String url, String dirPath, String suffix)
            throws IOException {
        if (httpClient == null) {
            throw new IllegalStateException("httpClient is null");
        }
        HttpGet httpGet = new HttpGet(url);
        httpGet.setConfig(requestConfig);
        CloseableHttpResponse response = null;
        File file = null;
        LOG.info("get file, url: {}", url);
        try {
            response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String sfx = null;
                Header h = response.getFirstHeader("Content-Type");
                if (h != null && h.getValue() != null) {
                    String ct = h.getValue();
                    int i = ct.indexOf('/');
                    if (i != -1) {
                        sfx = ct.substring(i + 1).toLowerCase();
                    }
                }
                if (StringUtils.isBlank(sfx)) {
                    sfx = suffix;
                }
                InputStream is = null;
                h = response.getFirstHeader("Content-Encoding");
                if (h != null && h.getValue() != null
                        && h.getValue().toLowerCase().contains("gzip")) {
                    is = new GZIPInputStream(entity.getContent());
                } else {
                    is = entity.getContent();
                }
                file =
                        new File(dirPath + File.separator + UUID.randomUUID().toString() + "."
                                + sfx);
                BufferedInputStream bis = new BufferedInputStream(is);
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                byte[] b = new byte[1024];
                int len = 0;
                while ((len = bis.read(b)) != -1) {
                    bos.write(b, 0, len);
                }
                bos.flush();
                bis.close();
                bos.close();
                LOG.info("get file finished");
            }
        } catch (IOException e) {
            LOG.error("getFile error, url: {}", url);
            throw e;
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }
    public static byte[] getFileData(String url) throws IOException {
        if (httpClient == null) {
            throw new IllegalStateException("httpClient is null");
        }
        HttpGet httpGet = new HttpGet(url);
        httpGet.setConfig(requestConfig);
        CloseableHttpResponse response = null;
        byte[] data = null;
        LOG.info("get fileData, url: {}", url);
        try {
            response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                Header h = response.getFirstHeader("Content-Encoding");
                if (h != null && h.getValue() != null
                        && h.getValue().toLowerCase().contains("gzip")) {
                    GZIPInputStream gzis = new GZIPInputStream(entity.getContent());
                    data = IOUtils.toByteArray(gzis);
                } else {
                    data = IOUtils.toByteArray(entity.getContent());
                }
                LOG.info("get fileData finished, return byte[], length {}", data.length);
            }
        } catch (IOException e) {
            LOG.error("get fileData error, url: {}", url);
            throw e;
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return data;
    }
    // 发送POST 请求
    public static String post(String url, Map<String, String> params) throws IOException {
        if (httpClient == null) {
            throw new IllegalStateException("httpClient is null");
        }
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        List<NameValuePair> formParams = new ArrayList<NameValuePair>();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            formParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }
        httpPost.setEntity(new UrlEncodedFormEntity(formParams, Consts.UTF_8));
        
        CloseableHttpResponse response = null;
        String result = null;
        LOG.info("post, url: {}, param: {}", url, String.valueOf(params));
        try {
            response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity);
                LOG.info("post finished, return: \n{}", result);
            }
        } catch (IOException e) {
            LOG.error("post error, url: {}, param: {}", url, String.valueOf(params));
            throw e;
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
    
    
    
    
    // 发送POST 请求
    public static String post(String url, Map<String, String> params,List<Header> headers) throws IOException {
        if (httpClient == null) {
            throw new IllegalStateException("httpClient is null");
        }
        
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        List<NameValuePair> formParams = new ArrayList<NameValuePair>();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            formParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }
        httpPost.setEntity(new UrlEncodedFormEntity(formParams, Consts.UTF_8));
        
        for(Header header:headers) {
            httpPost.addHeader(header);
        }
        
        CloseableHttpResponse response = null;
        String result = null;
        LOG.info("post, url: {}, param: {}", url, String.valueOf(params));
        try {
            response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity);
                LOG.info("post finished, return: \n{}", result);
            }
        } catch (IOException e) {
            LOG.error("post error, url: {}, param: {}", url, String.valueOf(params));
            throw e;
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
    
    public static String postJson(String url, String json) throws IOException {
        if (httpClient == null) {
            throw new IllegalStateException("httpClient is null");
        }
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        httpPost.setEntity(new StringEntity(json, Consts.UTF_8));
        httpPost.setHeader("Content-type", "application/json");
        CloseableHttpResponse response = null;
        String result = null;
        LOG.info("postJson, url: {}, json: {}", url, json);
        try {
            response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity);
                LOG.info("postJson finished, return: \n{}", result);
            }
        } catch (IOException e) {
            LOG.error("postJson error, url: {}, json: {}", url, json);
            throw e;
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
    
    
    public static String postJson(String url, String json,List<Header>headers) throws IOException {
        if (httpClient == null) {
            throw new IllegalStateException("httpClient is null");
        }
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        httpPost.setEntity(new StringEntity(json, Consts.UTF_8));
        httpPost.setHeader("Content-type", "application/json");
        for(Header header:headers) {
            httpPost.addHeader(header);
        }
        
        CloseableHttpResponse response = null;
        String result = null;
        LOG.info("postJson, url: {}, json: {}", url, json);
        try {
            response = httpClient.execute(httpPost);
            
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity);
                LOG.info("postJson finished, return: \n{}", result);
            }
        } catch (IOException e) {
            LOG.error("postJson error, url: {}, json: {}", url, json);
            throw e;
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
    
    public static String postXml(String url, String xml) throws IOException {
        if (httpClient == null) {
            throw new IllegalStateException("httpClient is null");
        }
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        httpPost.setEntity(new StringEntity(xml, Consts.UTF_8));
        httpPost.setHeader("Content-type", "text/xml");
        CloseableHttpResponse response = null;
        String result = null;
        LOG.info("postXml, url: {}, xml: {}", url, xml);
        try {
            response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity);
                LOG.info("postXml finished, return: \n{}", result);
            }
        } catch (IOException e) {
            LOG.error("postXml error, url: {}, xml: {}", url, xml);
            throw e;
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}