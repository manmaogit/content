package com.common.vo.page;

import com.alibaba.fastjson.annotation.JSONField;

public class Pagination {

    private Integer pageSize = 10;
    private Integer pageNo = 1;
    private Integer totalCount;
    private Integer totalPage;

    public Pagination() {
    	
    }

    public Pagination(Integer pageSize, Integer pageNo) {
        setPageSize(pageSize);
        setPageNo(pageNo);
    }

    public void setPageSize(Integer pageSize) {
        if (pageSize != null) {
            this.pageSize = pageSize;
        }
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageNo(Integer pageNo) {
        if (pageNo != null) {
            this.pageNo = pageNo;
        }
    }

    public Integer getPageNo() {
        return pageNo;
    }

    @JSONField(serialize = false)
    public Integer getStart() {
        return pageNo < 1 ? 0 : (pageNo - 1) * pageSize;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
        totalPage = totalCount / pageSize + (totalCount % pageSize > 0 ? 1 : 0);
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public Integer getTotalPage() {
        return totalPage;
    }
}
