package com.common.vo.page;

import java.util.Collections;
import java.util.List;

public class PageData<T> extends Pagination {
    
    private List<T> list;
    
    public PageData() {
        
    }
    
    public static <T> PageData<T> newInstance(Pagination p, Integer totalCount, List<T> list) {
        PageData<T> instance = new PageData<T>();
        instance.setPageSize(p.getPageSize());
        instance.setPageNo(p.getPageNo());
        instance.setTotalCount(totalCount);
        instance.setList(list);
        return instance;
    }
    
    public List<T> getList() {
        if (list == null) {
            return Collections.emptyList();
        }
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
