package com.common.vo;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.stereotype.Component;
/**
 * 
 * Presto 实体字段注解，可以用此注解标注实体的字段在数据库表中字段的名称
 * 
 * 
 * @author manmao
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface PrestoFiled {
    public String value() default "";
}