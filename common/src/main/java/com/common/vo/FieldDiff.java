package com.common.vo;


public class FieldDiff {
    
    public FieldDiff(String fieldName,String localValue,String remoteValue) {
        this.fieldName=fieldName;
        this.localValue=localValue;
        this.remoteValue=remoteValue;
    }
    
    private String fieldName;
    private String localValue;
    private String remoteValue;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getLocalValue() {
        return localValue;
    }

    public void setLocalValue(String localValue) {
        this.localValue = localValue;
    }

    public String getRemoteValue() {
        return remoteValue;
    }

    public void setRemoteValue(String remoteValue) {
        this.remoteValue = remoteValue;
    }
}