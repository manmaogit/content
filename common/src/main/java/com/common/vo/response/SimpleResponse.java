package com.common.vo.response;

/**
 * <pre>
 * {
 *   "data":[],
 *   "code":200,
 *    "msg":"Successfully call the api"
 * }
 * </pre>
 * 
 * @author yliu
 * @date 23 Jun, 2015 5:35:31 pm
 * @version v1.0
 */
public class SimpleResponse<T> extends ResponseBase {
    /* The payload is simple object */
    private T data;
    
    public SimpleResponse() {
    }
    
    public SimpleResponse(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
