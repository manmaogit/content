package com.common.vo.response;

/**
 * 
 * @author yliu
 * @date 23 Jun, 2015 5:16:18 pm
 * @version v1.0
 */
public class WebPagedPayload {
    private Integer totalCount; // The total record number

    private Integer currentPage; // current page No

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

}
