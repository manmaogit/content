package com.common.vo.response;

/**
 * 
 * the customized code will no be included this class and it's value should greater than
 * <b>1000</b>.
 * <p>
 * This class only predefined some common codes that litter than <b>1000</b>
 * 
 * @author yliu
 * @date 15 Jun, 2015 5:54:23 pm
 * @version v1.0
 */
public class ResponseCode {
    
    /** The success code */
    public static int SUCCESS = 200;

    /** The args passed from client is not valid */
    public static int INVALID_ARGS = 400;
    
    /** Has no permission to make the call */
    public static int INVALID_AUTH = 401;

    /** The erros/exception occurs when call the biz logic */
    public static int INTERNAL_ERROR = 500;
    
    /* ---------------defined for this application--------------- */
    /** invalid token */
    public static int INVALID_TOKEN = 1000;
    
    /** have no this story */
    public static int NOEXIST_STORY = 1001;
    
    /** you can not modify this story, because it's not yours */
    public static int CANNOT_MODIFY_STORY = 1002;
    
    /** you can not delete this story, because it's not yours */
    public static int CANNOT_DELETE_STORY = 1003;
}
