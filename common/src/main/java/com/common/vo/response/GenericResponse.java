package com.common.vo.response;

public class GenericResponse extends ResponseBase {
    
    /* The response real payload */
    
    private Object data;
    
    public GenericResponse() {
    }
    
    public GenericResponse(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
