package com.common.vo.response;

import com.common.vo.page.PageData;

public class PageResponse<T> extends ResponseBase {
    
    private PageData<T> data;
    
    public PageResponse() {
    }
    
    public PageResponse(PageData<T> data) {
        this.data = data;
    }

    public PageData<T> getData() {
        return data;
    }

    public void setData(PageData<T> data) {
        this.data = data;
    }
}
