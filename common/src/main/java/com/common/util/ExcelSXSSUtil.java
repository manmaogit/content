package com.common.util;

import java.lang.reflect.Field;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
public class ExcelSXSSUtil {
   
    public static SXSSFWorkbook newSXSSInstance() {
        return new SXSSFWorkbook(5000); // keep 5000 rows in memory, exceeding rows will be flushed to disk
    }
    
    /**
     * 将list写入单元格
     * @param list
     * @param ws
     * @return
     */
    public static void  createSheet(List<?> list,Workbook workbook,String sheetName) {
       try {
            Sheet sheet=workbook.createSheet(sheetName);
            int rowIndex = 0;
            for (Object object : list) {
                
                Row row = sheet.createRow(rowIndex);
                
                Class<? extends Object> clasz = object.getClass();
                Field[] fields = clasz.getDeclaredFields();
                int columnIndex = 0;
                for (Field field : fields) {
                    String str=BeanUtils.getProperty(object, field.getName());
                    Cell cell=row.createCell(columnIndex++);
                    cell.setCellValue(str);
                }
                rowIndex++;
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
       
    }
	
	/**
     * 将list写入单元格,增加头部
     * @param list
     * @param ws
     * @param header
     * @return
     */
    public static void  createSheet(List<?> list,Workbook workbook,String sheetName,List<String> header) {
       try {
           
            int rowIndex=0,columnIndex= 0;
            Row row = null;
            Sheet sheet=workbook.createSheet(sheetName);
            
            //header
            row = sheet.createRow(rowIndex++);
            for(String item:header) {
                Cell cell=row.createCell(columnIndex++);
                cell.setCellValue(item);
            }
            
            //content
            for (Object object : list) {
                row = sheet.createRow(rowIndex++);
                Class<? extends Object> clasz = object.getClass();
                Field[] fields = clasz.getDeclaredFields();
                columnIndex = 0;
                for (Field field : fields) {
                    String str=BeanUtils.getProperty(object, field.getName());
                    Cell cell=row.createCell(columnIndex++);
                    cell.setCellValue(str);
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
       
    }
	
    
}