package com.common.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 */
public class SpringUtil implements ApplicationContextAware {

    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        SpringUtil.context = context;
    }

    @SuppressWarnings("all")
    public static <T> T getBean(String beanName) {
        return (T) (context == null ? null : context.getBean(beanName));
    }

    @SuppressWarnings("all")
    public static <T> T getBean(Class<T> clz) {
        return context == null ? null : context.getBean(clz);
    }

    public static String[] getBeanDefinitionNames() {
        return context.getBeanDefinitionNames();
    }
}
