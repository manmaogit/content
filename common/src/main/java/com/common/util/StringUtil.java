package com.common.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by maoman on 16-11-28.
 */
public class StringUtil {
    public static Long parserToLong(String value,Long defaultValue){
        if(StringUtils.isNotBlank(value)){
            try {
                return Long.valueOf(value);
            } catch (Exception e) {
                return defaultValue;
            }
        }else {
            return defaultValue;
        }
    }
}
