package com.common.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Value;

public class PrestoConnFatory{
    
    @Value("${presto.url}")
    private String url;
    
    @Value("${presto.user}")
    private String user;
    
    private Connection   connection;
    
    
    public Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.facebook.presto.jdbc.PrestoDriver");  
        if(connection==null || connection.isClosed()) {
            Properties info=new Properties();
            info.setProperty("user", user);
      
            connection = DriverManager.getConnection(url, info);
        }
        return connection;
    }
    
    
    public  ResultSet getResultSet(Connection conn,String sql) throws SQLException{
        Statement sat=conn.createStatement();
        ResultSet set=sat.executeQuery(sql);
        return set;
        
    }
  
}