package com.common.util;

import com.common.vo.FieldDiff;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

public class ClassUtils {
    private static HashSet<String> BASE_TYPE_SET=new HashSet<>();
    static {
        BASE_TYPE_SET.add("java.lang.Boolean");
        BASE_TYPE_SET.add("java.lang.Character");
        BASE_TYPE_SET.add("java.lang.Byte");
        BASE_TYPE_SET.add("java.lang.Short");
        BASE_TYPE_SET.add("java.lang.Integer");
        BASE_TYPE_SET.add("java.lang.Long");
        BASE_TYPE_SET.add("java.lang.Float");
        BASE_TYPE_SET.add("java.lang.Double");
        BASE_TYPE_SET.add("java.lang.String");
        BASE_TYPE_SET.add("java.lang.Void");
        BASE_TYPE_SET.add("java.util.Date");


        BASE_TYPE_SET.add("boolean");
        BASE_TYPE_SET.add("char");
        BASE_TYPE_SET.add("byte");
        BASE_TYPE_SET.add("short");
        BASE_TYPE_SET.add("int");
        BASE_TYPE_SET.add("long");
        BASE_TYPE_SET.add("float");
        BASE_TYPE_SET.add("double");
        BASE_TYPE_SET.add("void");


    }


    /**
     *
     * 比较两个项目的不同字段
     *
     * @param upLevelField 上层 字段 ，顶层字段则上层字段为空
     * @param local 本地数据
     * @param remote 线上数据
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<FieldDiff> compareObject(String upLevelField, Object local, Object remote){

        List<FieldDiff> diffs=new ArrayList<>();

        if(local==null || remote==null)  //如果为空，返回
            return diffs;

        if(BASE_TYPE_SET.contains(local.getClass().getTypeName())) { //是否为基础类型
            if(!org.apache.commons.lang3.StringUtils.equals(local.toString(), remote.toString())) {
                diffs.add(new FieldDiff(upLevelField, local.toString(), remote.toString()));
            }
            return diffs;
        }

        Field[] fields = local.getClass().getDeclaredFields();
        for(Field field:fields) {
            try {
                if(Modifier.isPublic(field.getModifiers())) continue; //过滤public 字段
                if(Modifier.isStatic(field.getModifiers())) continue; //过滤static 字段

                if(BASE_TYPE_SET.contains(field.getGenericType().getTypeName())) { //直接比较类型

                    String localValue= BeanUtils.getProperty(local, field.getName());
                    String remoteValue=BeanUtils.getProperty(remote, field.getName());
                    if(!StringUtils.equals(localValue, remoteValue)) {
                        diffs.add(new FieldDiff(upLevelField+"-"+field.getName(), localValue, remoteValue));
                    }

                }else if(field.getType().isAssignableFrom(List.class)){      // list类型

                    List<Object> tmp1 =(List<Object>)getFieldValue(local,field.getName());
                    List<Object> tmp2 =(List<Object>)getFieldValue(remote,field.getName());

                    if(CollectionUtils.isNotEmpty(tmp1) && CollectionUtils.isNotEmpty(tmp2)) {
                        int min=tmp1.size()<tmp2.size()?tmp1.size():tmp2.size();
                        for(int i=0;i<min;i++) {
                            diffs.addAll(compareObject(field.getName()+"-"+i,tmp1.get(i),tmp2.get(i)));
                        }
                    }

                }else if(field.getType().isAssignableFrom(Map.class)){

                }else if(field.getType().isAssignableFrom(Set.class)) {

                }else if(field.getType().isArray()) {

                }else { //自定义类型
                    Object tmp1 =getFieldValue(local,field.getName());
                    Object tmp2 =getFieldValue(remote,field.getName());
                    diffs.addAll(compareObject(field.getName(),tmp1,tmp2));
                }

            }catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }

        return diffs;
    }

    @Deprecated
    private static Object getFieldValueByName( Object o,String fieldName)
    {
        try
        {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[] {});
            Object value = method.invoke(o, new Object[] {});
            return value;
        } catch (Exception e){
            System.out.println("属性不存在");
            return null;
        }
    }

    /**
     *
     * @param object
     * @param propertyName
     * @return
     */
    public static Object getFieldValue(Object object, String propertyName) {
        try {
            Field field = object.getClass().getDeclaredField(propertyName);
            field.setAccessible(true);
            return field.get(object);
        }catch (Exception e) {
            System.out.println("属性不存在");
            return null;
        }
    }
}