package com.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.common.vo.PrestoFiled;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Component;


public class ResultSetMapperUtil<T> {
    
    
    /**
     * ResultSet结果集数据转化成clasz对象list
     * 
     * @param rs
     * @param clasz
     * @return
     */
    public List<T> mapRersultSetToObjectList(ResultSet rs, Class<T> clasz) throws SQLException,
            InstantiationException,
            IllegalAccessException,
            InvocationTargetException {

        List<T> list=new ArrayList<>();
        
        while (rs.next()) {
            
            T e=(T)clasz.newInstance(); //构造实例
            Field[] fields=clasz.getDeclaredFields();
            
            //循环为field赋值
            for(Field field:fields) {
                PrestoFiled prestoFiled=field.getAnnotation(PrestoFiled.class);
                String cloumnName=null;
                if(prestoFiled != null) {   //注解不为空
                    cloumnName=prestoFiled.value();
                }else {                     //注解为空，转换类对象Field名字为全部小写字段。
                    cloumnName=field.getName().toLowerCase();
                }
                Object columnValue = rs.getObject(cloumnName);
               
                BeanUtils.setProperty(e, field.getName(), columnValue); //设置字段值
            }
            
            list.add(e);
        }
        
        return list;
    }
    
    
    /**
     * ResultSet结果集数据转化成clasz对象
     * 
     * @param rs
     * @param clasz
     * @return
     */
    
    public  T mapRersultSetToObject(ResultSet rs,Class<T> clasz) throws SQLException, InstantiationException, IllegalAccessException, InvocationTargetException{
        
        if (rs.next()) {
            T e=(T)clasz.newInstance(); //构造实例
            Field[] fields=clasz.getFields();
           
            //循环为field赋值
            for(Field field:fields) {
                
                PrestoFiled prestoFiled=field.getAnnotation(PrestoFiled.class);
                
                String cloumnName=null;
                
                if(prestoFiled != null) {   //注解不为空
                    cloumnName=prestoFiled.value();
                }else {
                    cloumnName=field.getName().toLowerCase();
                }
                
                Object columnValue = rs.getObject(cloumnName);  
                BeanUtils.setProperty(e, field.getName(), columnValue); //设置字段值
            }
            return e;
            
        }
        return null;
    }
}