package com.common.util;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/****
 * 支持简单的xml解析
 * 
 * @author maoman
 *
 */

public class XmlUtil {
    
    /**
     * 把xml流转化成Map
     * @param inputStream xml流
     * @return map
     * @throws Exception
     */
    public static Map<String, String> parseStreamXml2Map(InputStream inputStream) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        SAXReader reader = new SAXReader();
        Document document = reader.read(inputStream);
        
        Element root = document.getRootElement();      // 得到xml根元素
        List<Element> elementList = root.elements();  // 得到根元素的全部子节点
        for (Element e : elementList) {                // 遍历全部子节点
            /*
                访问子节点
                List<Attribute> attributeList=e.attributes();
                for(Attribute attr:attributeList){
                    System.out.println(attr.getName()+" : "+attr.getValue());
                 }
             */
            map.put(e.getName(), e.getText());
        }
        
        inputStream.close();
        inputStream = null;
        return map;
    }
    
    
    /**
     * 
     * 把xml字符串转化成
     * 
     * @param xml
     * @return map
     * @throws Exception
     * 
     */
    public static Map<String, String> parseStringXml2Map(String xml) throws Exception{      
        ByteArrayInputStream inputStream=new ByteArrayInputStream(xml.getBytes());      
        return parseStreamXml2Map(inputStream);
    }
    
    
    
    /**
     * 
     * 把xml文件转化成Map
     * 
     * @param path
     * @return
     * @throws Exception 
     * @throws FileNotFoundException 
     */
    public static  Map<String, String> parseFileXml2Map(String filePath) throws FileNotFoundException, Exception{  
        File file=new File(filePath);
        return parseStreamXml2Map(new FileInputStream(file));  
    }
    
    
    
    /**
     * 将对象转化成xml字符串
     * @param object 对象
     * @param root   根节点名称
     * 
     * @return
     */
    public static String objectToXml(Object object,String root) {
        xstream.alias(root, object.getClass());
        return xstream.toXML(object);
    }
    
    /**
     * 
     * 扩展xstream，使其支持CDATA块
     * 
     */
    private static XStream xstream = new XStream(new XppDriver() {
        public HierarchicalStreamWriter createWriter(Writer out) {
            return new PrettyPrintWriter(out) {
                
                // 对全部xml节点的转换都添加CDATA标记
                boolean isCdata = true;
                /**
                 * @param name ,节点名字
                 */
                @Override
                public void startNode(String name, Class clazz) {
                    super.startNode(name, clazz);
                    
                    if (name.equals("xxxx")) { //如果根节点名字是xxxx,
                        isCdata = false;
                    } else {
                        isCdata = true;
                    }
                }
                
                @Override
                protected void writeText(QuickWriter writer, String text) {
                    if (isCdata) { // 加<![CDATA[XXX]]>
                        writer.write("<![CDATA[");
                        writer.write(text);
                        writer.write("]]>");
                    } else { // 不加
                        writer.write(text);
                    }
                }
            };
        }
    });
    
    
   
    
   

}
