package com.content.persitence.dao;

import com.content.persitence.entity.UserEntity;
import org.apache.ibatis.annotations.Param;

/**
 * Created by maoman on 16-11-15.
 */
public interface UserDao {
    UserEntity getUser(@Param("id") Integer id);
    UserEntity queryUserByName(@Param("name") String userName);
}
