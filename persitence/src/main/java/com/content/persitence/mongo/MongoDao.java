package com.content.persitence.mongo;

import java.lang.reflect.Field;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.data.mongodb.core.MongoOperations;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public abstract class MongoDao {
    private static final String ID_COLLECTION = "id_generator";

    @Resource
    protected MongoOperations mongo;

    public Long nextId(String collectionName) {
        DBObject obj =
                mongo.getCollection(ID_COLLECTION).findAndModify(new BasicDBObject("coll", collectionName), null, null,
                        false, new BasicDBObject("$inc", new BasicDBObject("id", 1L)), true, true);
        return (Long) obj.get("id");
    }

    public static <T> T toEntity(DBObject dbo, Class<T> entityClazz) {
        if (dbo == null) {
            return null;
        }

        T entity = null;
        try {
            entity = entityClazz.newInstance();

            Field[] fields = entityClazz.getDeclaredFields();
            for (Field field : fields) {
                String fn = field.getName();
                Object fv = dbo.get(fn);
                if (fv != null) {
                    BeanUtils.setProperty(entity, fn, fv);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }
    
    public static DBObject buildFields(String... fieldNames) {
        DBObject fields = new BasicDBObject();
        for (String fn : fieldNames) {
            fields.put(fn, 1);
        }
        return fields;
    }
}