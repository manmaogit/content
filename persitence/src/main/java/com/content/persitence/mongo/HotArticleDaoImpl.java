package com.content.persitence.mongo;

import com.common.vo.page.Pagination;
import com.content.persitence.entity.HotArticle;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.QueryOperators;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

@Repository
public class HotArticleDaoImpl extends MongoDao implements HotArticleDao {
    
    protected static final String COLLECTION = "sp_hotarticle";
    
    //保存
    @Override
    public void insert(HotArticle hotArticle) {
        hotArticle.setArtId(nextId(COLLECTION));
        mongo.insert(hotArticle,COLLECTION);
    }
    
    //根据id更新
    @Override
    public void update(HotArticle hotArticle) {

        DBObject query = new BasicDBObject("artId", hotArticle.getArtId());

        DBObject o = new BasicDBObject();
        mongo.getConverter().write(hotArticle, o);

        mongo.getCollection(COLLECTION).update(query, new BasicDBObject("$set", o));
      //有则更新，没有则插入
      //mongo.getCollection(COLLECTION).update(query, new BasicDBObject("$set", o),true,true);
        
    }
    
    //更具url md5更新
    @Override
    public void updateByUrlMd5(HotArticle hotArticle) {

        DBObject query = new BasicDBObject("urlMd5", hotArticle.getUrlMd5());

        DBObject o = new BasicDBObject();
        mongo.getConverter().write(hotArticle, o);

        mongo.getCollection(COLLECTION).update(query, new BasicDBObject("$set", o));
        
    }
    
    //分页查询，带条件
    @Override
    public List<HotArticle> findListByParam(Date beginTime, Date endTime, Integer siteId, Integer sourceId, String url, Pagination page){
        
        endTime= DateUtils.addDays(endTime, 1);
        
        DBObject query =
                new BasicDBObject("createdTime",
                        new BasicDBObject(QueryOperators.GTE, beginTime).append(QueryOperators.LTE,
                                endTime));
        
        if (sourceId != null && sourceId !=0) {
            query.put("sourceId", sourceId);
           
        }
        
        if (siteId != null && siteId !=0) {
            query.put("siteId", siteId);
            
        }
        
        if (StringUtils.isNotBlank(url)) {
            query.put("url", Pattern.compile("^.*" + url + ".*$", Pattern.CASE_INSENSITIVE));
            
        }
        
        DBObject fields =
                buildFields("artId","siteId","hotSourceId","sourceUrl","newId", "url", "urlMd5","createdTime","status");
       
        DBCursor cursor =
                mongo.getCollection(COLLECTION).find(query, fields).skip(page.getStart())
                .limit(page.getPageSize()).sort(new BasicDBObject("createdTime",-1));
   
        
        List<HotArticle> result = new LinkedList<HotArticle>();
        while (cursor.hasNext()) {
            DBObject o = cursor.next();
            result.add(toEntity(o, HotArticle.class));
        }
        
        
        return result;
    }

  
    //删除一个时间点以前的数据
    @Override
    public void deleteAllBefore(Date time) {
       DBObject query =
                new BasicDBObject("createdTime",
                        new BasicDBObject(QueryOperators.LT, time));
       mongo.getCollection(COLLECTION).remove(query);
    
    }

    //统计
    @Override
    public Long findCount(Date beginTime, Date endTime, Integer siteId, Integer sourceId, String url) {
        endTime=DateUtils.addDays(endTime, 1);
        
        DBObject query =
                new BasicDBObject("createdTime",
                        new BasicDBObject(QueryOperators.GTE, beginTime).append(QueryOperators.LTE,
                                endTime));
        
        if (sourceId != null) {
            query.put("sourceId", sourceId);
        }
        
        if (siteId != null && siteId !=0) {
            query.put("siteId", siteId);
        }
        
        if (StringUtils.isNotBlank(url)) {
            query.put("url", Pattern.compile("^.*" + url + ".*$", Pattern.CASE_INSENSITIVE));
        }
        
        Long count = mongo.getCollection(COLLECTION).count(query);
        return count;
    }

}