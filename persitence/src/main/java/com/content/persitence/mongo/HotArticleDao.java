package com.content.persitence.mongo;

import com.common.vo.page.Pagination;
import com.content.persitence.entity.HotArticle;

import java.util.Date;
import java.util.List;

public interface HotArticleDao {
    
    void insert(HotArticle hotArticle);
    
    void update(HotArticle hotArticle);
    
    public void updateByUrlMd5(HotArticle hotArticle);
    
    List<HotArticle> findListByParam(Date beginTime, Date endTime, Integer siteId, Integer sourceId, String url, Pagination page);
    
    Long findCount(Date beginTime, Date endTime,Integer siteId,Integer sourceId,String url);
    
    void deleteAllBefore(Date time);
}