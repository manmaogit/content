package com.content.persitence.es.common;

/**
 * @author kelong
 * @date 10/11/16.
 */
public interface EsModel {
    String getDocId();

    String getJson();
}
