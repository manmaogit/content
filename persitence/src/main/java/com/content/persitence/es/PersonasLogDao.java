package com.content.persitence.es;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface PersonasLogDao {

	/**
	 * 查询指定日期范围的用户id
	 * @param startDate
	 * @param endDate
	 * @return 用户id
	 */
	Set<String> findActiveUserId(Long startDate, Long endDate);
	
	/**
	 * 查询指定日期范围特定用户的特定行为日志
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @param actions
	 * @return 日志对象列表
	 */
	List<Map<String, String>> findUserLogInfo(Set<String> userIds, Long startDate, Long endDate, Set<String> actions);
	
	
	/**
	 * 按照分类统计每个分类各个行为的数量
     * @param actions
	 * @param startDate
	 * @param endDate
	 * @return categoryCode-action-count
	 */
	Map<String, Map<String, Long>> groupCategoryActions(Set<String> actions,Long startDate,Long endDate);
	
	
	/**
	 * 统计活跃用户
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	Long countActiveUsers(Long startDate,Long endDate);
	
	/**
    * 使用指定的分词器获取分词查询结果
    * @param index 索引
    * @param text 要分词的内容
    * @param analyzer 分词器
    * @return 分词结果
    */
	void analyzeSearch(String index,  String text,String analyzer);
}
