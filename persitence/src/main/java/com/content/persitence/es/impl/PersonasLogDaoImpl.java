package com.content.persitence.es.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.content.persitence.es.PersonasLogDao;
import com.content.persitence.es.common.EsClientFactory;
import com.content.persitence.es.common.EsDaoSupport;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.indices.analyze.AnalyzeRequestBuilder;
import org.elasticsearch.action.admin.indices.analyze.AnalyzeResponse;
import org.elasticsearch.action.admin.indices.analyze.AnalyzeResponse.AnalyzeToken;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class PersonasLogDaoImpl extends EsDaoSupport implements PersonasLogDao {

	private static final Logger logger = LoggerFactory.getLogger(PersonasLogDaoImpl.class);

	private static final String log_index = "log_sys_v2";
	
	private static final String log_type = "request_log";

	private static final String active_user_index = "log_sys_v2";
	private static final String active_user_type = "active_user_log_day";
	
	private static ThreadLocal<String> scrollIdMap = new ThreadLocal<>();
	

	private static Integer ACTIVE_USER_PAGE_SIZE=1000; //活跃用户分页条数
	
	private static Integer DEFAULT_SIZE=1000000; //默认查询返回条数
	
	private static Integer DEFAULT_KEEPALIVE_TIME=60*5; //默认5分钟
	
	private static TimeValue ACTIVE_USER_SCORLL_KEEPALIVE_TIME=TimeValue.timeValueMinutes(10); //滚屏游标缓存时间　10分钟　
	
	public Set<String> findActiveUserId(Long startDate, Long endDate) {
	    
	    String scrollId = scrollIdMap.get();
	    
	    Set<String> uidsSet = new HashSet<String>();
		SearchHit[] hits = null;
		
		if(StringUtils.isBlank(scrollId)){ //游标快照过期
		    BoolQueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.rangeQuery("firstTime").from(startDate).to(endDate));
		    
	        SearchRequestBuilder request = newSearch(active_user_index, active_user_type).setQuery(query).addSort("uid", SortOrder.ASC)
	                .addField("uid").setScroll(ACTIVE_USER_SCORLL_KEEPALIVE_TIME).setSize(ACTIVE_USER_PAGE_SIZE).setSearchType(SearchType.COUNT);
	        
	        logger.debug("active userid find sql: {}", request.toString());
	        
	        SearchResponse scrollResponse = request.execute().actionGet();
            scrollIdMap.set(scrollResponse.getScrollId());
            
            hits = scrollResponse.getHits().hits();
            
		}else { //游标快照没过期,直接获取数据
		    Client client = EsClientFactory.getEsTransportClient();
            SearchResponse scrollResponse = client.prepareSearchScroll(scrollId).setScroll(ACTIVE_USER_SCORLL_KEEPALIVE_TIME).execute().actionGet();
            hits = scrollResponse.getHits().hits();
        }

        for (SearchHit hit : hits) {
            String userId = (String) hit.getFields().get("uid").getValues().get(0);
            if (StringUtils.isNotBlank(userId)) {
                uidsSet.add(userId);
            }
        }


        
		return uidsSet;

	}

	@Override
	public List<Map<String, String>> findUserLogInfo(Set<String> userIds, Long startDate, Long endDate, Set<String> actions) {
	    
		//Map里装的是每条日志需要的各个属性，List其实就是装的符合条件的日志
		List<Map<String, String>> logList = new ArrayList<Map<String, String>>();
		
		BoolQueryBuilder query = QueryBuilders.boolQuery()
				.must(QueryBuilders.rangeQuery("timestamp").from(startDate).to(endDate))
				.must(QueryBuilders.existsQuery("realCategoryCode"))
				.mustNot(QueryBuilders.termQuery("realCategoryCode","10000001"))
				.must(QueryBuilders.termsQuery("action", actions.toArray()));
		
		if(CollectionUtils.isNotEmpty(userIds)){
			query.must(QueryBuilders.termsQuery("uid", userIds.toArray()));
		}
		
		SearchRequestBuilder request = newSearch(log_index, log_type).addField("uid").addField("action").addField("realCategoryCode")
		        .setQuery(query).setScroll(TimeValue.timeValueSeconds(DEFAULT_KEEPALIVE_TIME)).setSize(DEFAULT_SIZE).addSort("uid",SortOrder.ASC);
		
		logger.debug("users log find sql: {}", request.toString());
		
		SearchResponse response = request.execute().actionGet();

		logger.debug("users log find result: {}", response.toString());
		

		SearchHit[] hits = response.getHits().getHits();

		for (SearchHit hit : hits) {
			
			Map<String, String> logitem = new HashMap<String, String>();
			
			String userId = (String)hit.getFields().get("uid").getValues().get(0);
			String categoryCode = (String)hit.getFields().get("realCategoryCode").getValues().get(0);
			String action = (String)hit.getFields().get("action").getValues().get(0);

			if (StringUtils.isNotBlank(userId) || StringUtils.isNotBlank(categoryCode) && StringUtils.isNotBlank(action)) {
				logitem.put("userId", userId);
				logitem.put("categoryCode", categoryCode);
				logitem.put("action", action);
				logList.add(logitem);
			}
		}
		
		return logList;
		
	}
	
	
    @Override
    public Map<String, Map<String, Long>> groupCategoryActions(Set<String> actions,Long startDate,Long endDate) {
        
        Map<String, Map<String, Long>>resultMap=new HashMap<String, Map<String,Long>>();
        
        SearchRequestBuilder searchBuilder = newSearch(log_index, log_type);
        
        /*　设置查询条件　*/
        BoolQueryBuilder query = QueryBuilders.boolQuery()
                .must(QueryBuilders.rangeQuery("timestamp").from(startDate).to(endDate))
                .must(QueryBuilders.termsQuery("action", actions));
                
        searchBuilder.setQuery(query);
        searchBuilder.setScroll(TimeValue.timeValueSeconds(DEFAULT_KEEPALIVE_TIME)).setSize(DEFAULT_SIZE);
        
        
        /*　设置聚合管道　*/
        TermsBuilder categoryTermsBuilder = AggregationBuilders.terms("categoryAgg").field("realCategoryCode").size(DEFAULT_SIZE);
        TermsBuilder actionTermsBuilder = AggregationBuilders.terms("actionAgg").field("action").size(DEFAULT_SIZE);
        categoryTermsBuilder.subAggregation(actionTermsBuilder);
        searchBuilder.addAggregation(categoryTermsBuilder);
        
        
        /*查询获取返回结果*/
        SearchResponse sr = searchBuilder.execute().actionGet();
        Map<String, Aggregation> aggMap = sr.getAggregations().asMap();
       
        
        /*聚合操作结果解析*/
        StringTerms categoryTerms = (StringTerms) aggMap.get("categoryAgg");
        
        Iterator<Bucket> categoryBucketIt = categoryTerms.getBuckets().iterator();
        
        while(categoryBucketIt.hasNext())
        {
            Bucket categoryBucket = categoryBucketIt.next();
            
            String categoryCode=(String)categoryBucket.getKey();
            
            Map<String, Long>actionMap=resultMap.get(categoryCode);
            if(actionMap == null){
                actionMap=new HashMap<String, Long>();
                resultMap.put(categoryCode, actionMap);
            }
            
            StringTerms actionTerms = (StringTerms) categoryBucket.getAggregations().asMap().get("actionAgg");
            Iterator<Bucket> actionBucketIt = actionTerms.getBuckets().iterator();
            
            while(actionBucketIt.hasNext())
            {
                Bucket actionBucket = actionBucketIt.next();
                
                String  action=(String)actionBucket.getKey();
                Long count=(Long)actionBucket.getDocCount();
                actionMap.put(action, count);
                
                logger.info(categoryBucket.getKey() + " : " +actionBucket.getKey() + " : " + actionBucket.getDocCount() +" ");
            }
            
        }
        
        return resultMap;
    }
    
    
    @Override
    public Long countActiveUsers(Long startDate, Long endDate) {
        
        SearchRequestBuilder searchBuilder = newSearch(active_user_index, active_user_type);
        
        /*设置查询范围*/
        BoolQueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.rangeQuery("firstTime").from(startDate).to(endDate));
        searchBuilder.setQuery(query).setScroll(TimeValue.timeValueSeconds(DEFAULT_KEEPALIVE_TIME)).setSize(DEFAULT_SIZE);
        
        /*查询获取返回结果*/
        SearchResponse searchResponse = searchBuilder.execute().actionGet();
        
      
        System.out.print('\n');
        
        return searchResponse.getHits().getTotalHits();
        
    }

	@Override
	public void analyzeSearch(String index,  String text,String analyzer) {
		IndicesAdminClient indicesAdminClient = EsClientFactory
				.getEsTransportClient().admin().indices();
		AnalyzeRequestBuilder analyzeRequestBuilder  = indicesAdminClient.prepareAnalyze(index, text);
		analyzeRequestBuilder.setAnalyzer(analyzer); //设置分词器
		// Analyzer（分析器）、Tokenizer（分词器）
		//获取分词结果
		List<AnalyzeToken> listAnalysis = analyzeRequestBuilder.execute().actionGet().getTokens();
		System.out.println(listAnalysis);

		
		
		for (AnalyzeResponse.AnalyzeToken token : listAnalysis) {
			System.out.println(token.getTerm());
			System.out.println(',');
			
			//根据分词，查询active_user_index的active_user_type
			BoolQueryBuilder query=QueryBuilders.boolQuery().should(QueryBuilders.queryStringQuery(token.getTerm()).field("search_keys_ik"));
			SearchRequestBuilder request = newSearch(active_user_index, active_user_type).setQuery(query);
			SearchResponse scrollResponse = request.execute().actionGet();
			SearchHit[] hits = scrollResponse.getHits().hits();
		}
		 
	}
	
    
    
    
    
    
    
	
	
}
