package com.content.persitence.entity;

import java.io.Serializable;
import java.util.Date;

public class HotArticle implements Serializable {

    private static final long serialVersionUID = -312854320019484371L;
    
    private Long     artId;   // mongdb自增id
    private Integer siteId;  // 网站id
    private Integer hotSourceId;// 热点二级内容源id
    private String  sourceUrl;  
    private Long    newId;   // 在sp_content表中id
    private String  url;    // 热点文章url
    private String  urlMd5; // url md5
    private Date createdTime; // 创建时间
    private Integer  status;  //０，未抓取，１，已抓取
    
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getUrlMd5() {
        return urlMd5;
    }

    public void setUrlMd5(String urlMd5) {
        this.urlMd5 = urlMd5;
    }

    public Long getNewId() {
        return newId;
    }

    public void setNewId(Long newId) {
        this.newId = newId;
    }

    public Long getArtId() {
        return artId;
    }

    public void setArtId(Long artId) {
        this.artId = artId;
    }


    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public Integer getHotSourceId() {
        return hotSourceId;
    }

    public void setHotSourceId(Integer hotSourceId) {
        this.hotSourceId = hotSourceId;
    }

}